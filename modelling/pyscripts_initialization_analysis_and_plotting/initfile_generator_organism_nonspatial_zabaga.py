#!/usr/bin/env python

# File generating initial configuration file when spatial localion of the seeds does not matter.

#Input parameters
############

delta=0;            # relative size of the initial fluctuations. To avoid having negative initial conditions, 0<delta<1
delta_targ=0;       # 

zerovars=[4,7]; 	# variable indices list starting with zero initial conditions (Note var 0 is ABA, 1 is GA, 2 is Target, 4 is GerminationTime)
size_seeds=1        # cell sizes

################

# Importing packages

import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from scipy.stats import truncnorm
import pylab
import os
import shutil
import pickle
import json

dirout='InitFiles/'
try:
        os.stat(dirout)
except:
        os.mkdir(dirout) 

#info= pickle.load(open('info.pkl'))
with open("info.json", "r") as f:
    info= json.load(f)

ga_ic=info['initial_conds_pars']['ga_ic']; # 
aba_ic=info['initial_conds_pars']['aba_ic']; #
target_ic=info['initial_conds_pars']['target_ic']; # 
thresholds_for_germination=info['initial_conds_pars']['thresholds_for_germination']; # 
start_ic_seed=info['initial_conds_pars']['start_ic_seed']; # 

num_seeds=info['initial_conds_pars']['num_seeds']; # number of seeds
num_realisations=info['initial_conds_pars']['num_realisations']	 # number of init files required	 

theta_zaba=info['model_pars']['theta_zaba']; # 
theta_zga=info['model_pars']['theta_zga']; #

delta_1aba=info['model_pars']['delta_1aba']; # 
delta_2ga=info['model_pars']['delta_2ga']; #

num_vars=info['model_pars']['num_vars']
dataname=info['strings']['init_filename']
initfilename=dataname;

# precursor fixed points for variables x0 and y0 

w0=aba_ic;
x0=ga_ic;
y0=target_ic;
z0=thresholds_for_germination; # threshold

num_coords=2 # 2 for 2D 
num_shapeparameters=1 # 1 when just providing 1 parameter about the geometry of the cell

num_zerovars=len(zerovars);

num_randvars=num_vars-num_zerovars;


################

def posy(i,num_seeds) :
    return 0

def posx(i,num_seeds) :
    return 0

def init_cell(i,num_seeds,size_seeds) :
    return [posx(i,num_seeds), posy(i,num_seeds), size_seeds]

###################

# Creating the geometry (coordinates and size) for the array of seeds.
geometry=[]
for ii in range(0,num_seeds,1) :
    geometry.append(init_cell(ii,num_seeds,size_seeds))

ic_geometry=np.array(geometry)

# Generation of a 0 column
zero_column=np.zeros(num_seeds)

num_allvars=num_coords+num_shapeparameters+num_vars

firstline=[num_seeds,num_allvars] # first line of the init files


data=[]

for jj in range(start_ic_seed-1,start_ic_seed+num_realisations-1,1) :

    # setting by default Gaussian random variables from -0.5 to 0.5 with mean=0 and SD=1.

    #a,b=-0.5,0.5    # limits of standard form of normal distribution (mean=0, SD=1)
    # low_lim, up_lim = (-0.5-0)/0.5, (0.5-0)/0.5 # converting limit values for a specific mean and normal distribution where a, b = (myclip_a - my_mean) / my_std, (myclip_b - my_mean) / my_std 

    rand_species=(1+delta*truncnorm.rvs(-1,1,loc=0,scale=1,size=[num_seeds,num_vars]));  # truncated normal distribution 
    rand_species_targ=(1+delta_targ*truncnorm.rvs(-1,1,loc=0,scale=1,size=[num_seeds,num_vars]));

    transp_rand_species=np.transpose(rand_species);
    transp_rand_species_targ=np.transpose(rand_species_targ);

    # Setting 0-initial conditions where needed

    for ii in range(0,num_zerovars,1) :
	    column=zerovars[ii];
	    transp_rand_species[column]=zero_column;

    # Other modifications
    special_column=0; #Column for the first variable aba_ic
    transp_rand_species[special_column]=w0*transp_rand_species[special_column];

    special_column=1; #Column for the second variable ga_ic
    transp_rand_species[special_column]=x0*transp_rand_species[special_column];

    special_column=2; #Column for the third variable target_ic
    transp_rand_species[special_column]=y0*transp_rand_species_targ[special_column];

    special_column=3; #Column for the fourth variable thresholds_for_germination
    transp_rand_species[special_column]=z0;

    special_column=5; #Column for the sixth variable NoiseOneaba
    transp_rand_species[special_column]=delta_1aba*theta_zaba*truncnorm.rvs(-1,1,loc=0,scale=1.0,size=[num_seeds]); #variation in theta is normally distributed
    
    special_column=6; #Column for the seventh variable NoiseTwoga
    transp_rand_species[special_column]=delta_2ga*theta_zga*truncnorm.rvs(-1,1,loc=0,scale=1.0,size=[num_seeds]);

    ic_species=np.transpose(transp_rand_species);
    ics=np.concatenate((ic_geometry,ic_species),axis=1)
            
    final_data=[firstline]+ics.tolist()

    lastlines=[]
    for j in range(0,len(final_data)) :
        line=[" ".join(str(i) for i in final_data[j])]
        lastlines.append(line[0])

        ini_dirout='InitFiles/'+dataname+'/'
        try:
                os.stat(ini_dirout)
        except:
                os.mkdir(ini_dirout) 

        fullinitfilename=ini_dirout+initfilename+'_ic_'+str(jj+1)+'.init'

        datafile = open (fullinitfilename,'w')
        datafile.writelines( "%s\n" % item for item in lastlines )

