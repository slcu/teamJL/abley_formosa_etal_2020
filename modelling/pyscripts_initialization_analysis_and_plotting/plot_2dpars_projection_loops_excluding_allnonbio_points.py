#!/usr/bin/env python

# NOTE: this script will not produce a pdf when there are no points to represent ('tosave approach')

# Importing packages###############
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio

matplotlib.rcParams['pdf.fonttype'] = 42

#import statistics

#############################################
fiz=1.77       # Figure size      # Figure size
fiz=2.05 # former size
fiz=3
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots

dirini=os.getcwd()

#with open("../info.json", "r") as f:
#    info= json.load(f)
#os.chdir("..")

with open("info.json", "r") as f:
    info= json.load(f)

#os.chdir("..")

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_parsexploration_path']
num_realisations=info['initial_conds_pars']['num_realisations']
#var_doses=info['strings']['var_doses']
string_sim=info['strings']['string_sim']
par1=info['varying_par']['par1']
par2=info['varying_par']['par2']
dimsweep=info['varying_par']['dimsweep']

var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']

get_bifdiag=info['plotting_pars']['get_bifdiag']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

bastime_ga=info['model_pars']['bastime_ga']

par1sweep=info['parsweep'][par1]
par2sweep=info['parsweep'][par2]

ymin=min(par1sweep)
ymax=max(par1sweep)
xmin=min(par2sweep)
xmax=max(par2sweep)

#if bastime_ga>0 :
#    if 'CTL_dataname_out' in locals():
#        colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']
#        CTL_filedat=CTL_dataname_out+'/dose_obs.dat' 
#        CTL_x = pd.read_csv(CTL_filedat,sep='\t',skip_blank_lines=True,names=colnames)

if 'CTL_main_parsexploration_path' in info['strings'] :
    CTL_dataname_out=info['strings']['CTL_main_parsexploration_path']

filedat=dataname_out+'/dose_obs.dat' 

if dimsweep==1 :
    colnames=['val_ic',par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size']
elif dimsweep==2 :
    colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']

if 'CTL_main_parsexploration_path' in info['strings'] :
    filedat_CTL=CTL_dataname_out+'/dose_obs.dat'
    x_CTL = pd.read_csv(filedat_CTL,sep='\t',skip_blank_lines=True,names=colnames)
    x_CTL=x_CTL.round(8) # rounding to prevent errors in matching
    x_CTL=x_CTL[(x_CTL[par2]>=xmin) & (x_CTL[par2]<=xmax) & (x_CTL[par1]>=ymin) & (x_CTL[par1]<=ymax)]
    x_CTL_excluded=x_CTL[(x_CTL['fraction_germinated_seeds'])>0]
    x_CTL_included=x_CTL[(x_CTL['fraction_germinated_seeds'])==0]

x = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=colnames)
x=x.round(8) # rounding to prevent errors in matching
x=x[(x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)]


x.cv_gt=x.cv_gt.replace(0, np.nan) # replacing CVs being 0 by nans.
#x_for_cv=x[(x.cv_gt>=0.03)] 
#x_for_mode=x[(x.mode>=1)]

if size(x_CTL_included)>0 :
    xxx=x
    m=transpose([x_CTL_included[par1].values,x_CTL_included[par2].values]);
    points_to_keep=np.unique(m,axis=0)

    x_clean=[]
    i=0
    for i in range(0,len(points_to_keep),1) :
        xaux=x[(x[par1]==points_to_keep[i][0]) & (x[par2]==points_to_keep[i][1])]
        if len(xaux)>0 :
            if len(x_clean)==0 :
                x_clean=xaux
            else :
                x_clean=pd.concat([x_clean,xaux])
    x=x_clean 
    x=x.reset_index()

if size(x_CTL_excluded)>0 :
    m_exc=transpose([x_CTL_excluded[par1].values,x_CTL_excluded[par2].values]);
    points_to_exclude=np.unique(m_exc,axis=0)

# excluding points that are non bio relevant
x=x[(x.ithtar==1) & (x.fthtar==1)]

# these plotting ranges are not used for the current script
pranges={'fraction_germinated_seeds':[0, 1],'cv_gt':[0, x['cv_gt'].max()],'mode_gt':[0, x['mode_gt'].max()],'mean_gt':[0, x['mean_gt'].max()]}
lpranges={'cv_gt':[-1.5, np.log10(x['cv_gt'].max())],'mode_gt':[-1, np.log10(x['mode_gt'].max())]} 
#lpranges={'cv_gt':[np.log10(x['cv_gt'].min()), np.log10(x['cv_gt'].max())],'mode_gt':[np.log10(x['mode_gt'].min()), np.log10(x['mode_gt'].max())]}
ics=x['val_ic'].unique()

labels={'fraction_germinated_seeds':'Germination fraction','mean_gt':'Mean germination time (A.U.)','added_ga':'Exogenous GA','added_aba':'Exogenous ABA','cv_gt':'CV of germination time ','q2_gt':'Median germination time (A.U.)','cvq_gt': 'IQR/median of germination time','mode_gt':'Mode germination time (A.U.)','size':'Number of germinated seeds','theta_zga':'GA threshold for integrator','theta_zaba':'ABA threshold for integrator'}

# Labels for paper
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds','theta_zga':r'$\theta_{I,GA}$','theta_zaba':r'$\theta_{I,ABA}$','seed_vol':'V','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','basal_target':'I Production'}
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds','theta_ga':r'$\theta_{GA}$','theta_aba':r'$\theta_{ABA}$','basal_aba':'ABA Production','basal_ga':'GA Production','deg_aba':'ABA Degradation','deg_ga':'GA Degradation','theta_zga':r'$\theta_{I,GA}$','theta_zaba':r'$\theta_{I,ABA}$','seed_vol':'V','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','basal_target':'I Production'}
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds'}
axlabels={'theta_ga':'I threshold \n for GA inhibition ' r'($\theta_{GA,I})$','theta_aba':'I threshold \n for ABA activation ' r'$(\theta_{ABA,I})$','basal_aba':'ABA production ' r'($\beta_{ABA}$)','basal_ga':'GA production \n' r'($\beta_{GA}$)','deg_aba':'ABA degradation ' r'($\nu_{ABA}$)','deg_ga':'GA degradation ' r'($\nu_{GA}$)','theta_zga': 'GA threshold for \n I degradation ' r'($\theta_{I,GA})$','theta_zaba': 'ABA threshold \n for I activation ' r'($\theta_{I,ABA})$','seed_vol':'1/(Noise intensity) ' r'($V$)','deg2_target':'GA-dependent \n I degradation ' r'($C_{I,GA}$)','deg_target': 'I degradation ' r'($\nu_{I})$','basal_target': 'I production ' r'($\beta_{I}$)'}
axlabels={'theta_ga':'I threshold \n for GA inhibition ' r'($\theta_{GA,I})$','theta_aba':'I threshold \n for ABA activation ' r'$(\theta_{ABA,I})$','basal_aba':'ABA production \n' r'($\beta_{ABA}$)','basal_ga':'GA production \n' r'($\beta_{GA}$)','deg_aba':'ABA degradation \n' r'($\nu_{ABA}$)','deg_ga':'GA degradation \n' r'($\nu_{GA}$)','theta_zga': 'GA threshold for \n I degradation ' r'($\theta_{I,GA})$','theta_zaba': 'ABA threshold \n for I activation ' r'($\theta_{I,ABA})$','seed_vol':'1/(Noise intensity) \n' r'($V$)','deg2_target':'GA-dependent \n I degradation ' r'($C_{I,GA}$)','deg_target': 'I degradation \n' r'($\nu_{I})$','basal_target': 'I production \n' r'($\beta_{I}$)','vmax_z':'ABA-dependent \n I activation' r'($C_{I,ABA}$)'}
filelabels={'fraction_germinated_seeds':'fr_germ_seeds','mean_gt':'mean_gt','q2_gt':'median_gt','cv_gt':'cv_gt','cvq_gt':'cvq_gt','mode_gt':'mode_gt','size':'gseedsnum'}


variables_to_plot=['fraction_germinated_seeds','mean_gt','cv_gt','mode_gt']
variables_to_plot=['fraction_germinated_seeds','cv_gt','mode_gt']

pars2=x[par2].unique()
pars1=x[par1].unique()

# Plotting figures
fig_size=(fiz,fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'

#rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Arial']
pylab.rcParams.update(params)

dirout = dataname_out+'/vs_1par_plots_biobio'  #creating sub-directory
try:
    os.stat(dirout)
except:
    os.mkdir(dirout)


partochange=par1
parctt=par2

parctts=x[parctt].unique()

xs_sweep=x[partochange].unique()
zs_sweep=x[parctt].unique()

#xs_sweep=info['parsweep'][partochange]
#zs_sweep=info['parsweep'][parctt]

parctts=zs_sweep


for jj in range(0,len(parctts),1) :                   #for loop for specific variables
    indx=jj
    print(str(parctt),'=', parctts[indx])

    for ii in range(0,len(variables_to_plot),1) :                   #for loop for specific variables
        tosave=0 # this is going to turn into 1 when we don't have errors (eg when there is at least 1 point to represent non-nan, etc)

        #print(ii)

        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        #plt.subplots_adjust(left=0.25, bottom=0.20, right=0.8, top=None, wspace=0.5, hspace=0.6)
        plt.subplots_adjust(left=0.25, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        var=variables_to_plot[ii]    #get name of variable 
        xmin=min(xs_sweep);
        xmax=max(xs_sweep)*1.1;
        ymin=0;ymax=max(x[var])*1.05;
        
        #scale='linear'
        scale='log'
        yss=[]
        for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
            ind=ics[jj]                   #seed_indices defined at start
            xs=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][partochange].values      #x values of time course for one seed
            ys=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][var].values      #ind is an index        
		# for plotting purposes we do this cut, such that there are no lines joining ponts that fall outside the panels
            if var=='cv_gt' :
		xxs=np.array(xs)
		yys=np.array(ys)
		xxs=xxs[yys>=0.03]
		yys=yys[yys>=0.03]
		xs=np.ndarray.tolist(xxs)
		ys=np.ndarray.tolist(yys)
 	    if var=='mode_gt' :
		xxs=np.array(xs)
		yys=np.array(ys)
		xxs=xxs[yys>=1]
		yys=yys[yys>=1]
		xs=np.ndarray.tolist(xxs)
		ys=np.ndarray.tolist(yys) 
            yss.extend(ys)
            plt.plot(xs,ys,'-o',linewidth=lw);

        plt.xlabel(axlabels[str(partochange)],fontsize=fz)
        plt.ylabel(labels[var],fontsize=fz)
        

        # If there are just nans or non-positive in the ys, let's not do the log-axis transformation to prevent errors
        yss=np.array(yss)
        nan_array = np.isnan(yss)
        not_nan_array = ~ nan_array
        array2 = yss[not_nan_array]
        
        logscale=0
        
        #print(yss)

        if array2.size>0 :
            pos_array=array2>0
            array2_pos=array2[pos_array]
            if array2_pos.size>0 :
                logscale=1
                plt.xscale(scale);
                tosave=1
        else :
            tosave=0

        #print('logscale',logscale)
        #plt.xticks(np.logspace(-2,2,num=5), (r"0",r"2",r"4",r"6",r"8",r"10"),fontsize=fz)
        
        #if partochange=='seed_vol' :
        #    plt.xticks(np.logspace(-2,2,num=5),fontsize=fz)
        #else :
        #    plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)
  
        plt.yticks(np.arange(0,end_plotting_time,step=ticks_separation),fontsize=fz)

        #plt.setp(ax.get_xticklabels(), fontsize=fz)
        #plt.xticks(np.arange(0,0.04,step=0.02))
        #ScalarFormatter(useOffset=True)

        rat = (ymax-ymin)/(xmax-xmin)

        if var=='cv_gt' and logscale==1 :
            plt.yscale('log');
            ymax=1
            ymin=0.03
            #ymin=0.01
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))
            plt.tight_layout()
            #plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)

        if var=='fraction_germinated_seeds' :
            ymax=max(x[var])*1.1
            rat = (ymax-ymin)/(log10(xmax)-log10(xmin))
            plt.yticks(np.arange(0,1.01,step=0.25),(r"0",r"25",r"50",r"75",r"100"),fontsize=fz)
            #plt.tight_layout()
            tosave=1
            gcf().set_size_inches(1.935,3.22)
        if var=='mean_gt' :
            ymax=end_plotting_time*0.95
            plt.tight_layout()
            tosave=1
            
        if var=='mode_gt' and logscale==1 :
            ymin=1
            #ymin=0.1
            plt.yscale('log');
            ymax=end_plotting_time*0.95
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))
            plt.tight_layout()
            
        #ax1.xaxis.grid(True, which='minor')
        
        plt.minorticks_off()
        #ax1.set_aspect(0.618/rat)
        ax1.set_aspect(1.0/rat)

        plt.subplots_adjust(bottom=0.5)
        xmin=1
        
        if not(isnan(ymax)) :
            plt.axis([xmin,xmax,ymin,ymax])
        
        xlim(left=1)
        if tosave==1 :
            fileout='_'+var
            fname=string_sim+par_string+"_dose"+fileout+"_bio.pdf"
            fname=var+'_vs_'+str(partochange)+'_'+str(parctt)+str(parctts[indx])+"_bio.pdf"

            fullfname=dirout+'/'+fname
            plt.savefig(fullfname, format=None)
        
        #plt.show()
        
        plt.close()

partochange=par2
parctt=par1

parctts=x[parctt].unique()

xs_sweep=x[partochange].unique()
zs_sweep=x[parctt].unique()

#xs_sweep=info['parsweep'][partochange]
#zs_sweep=info['parsweep'][parctt]

parctts=zs_sweep

for jj in range(0,len(parctts),1) :                   #for loop for specific variables
    indx=jj
    print(str(parctt),'=', parctts[indx])

    for ii in range(0,len(variables_to_plot),1) :                   #for loop for specific variables
        tosave=0
        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        plt.subplots_adjust(left=0.25, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        var=variables_to_plot[ii]    #get name of variable 
        xmin=min(xs_sweep);
        xmax=max(xs_sweep)*1.1;
        ymin=0;ymax=max(x[var])*1.05;
        
        scale='log'
        yss=[]
        #scale='linear'
        for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
            ind=ics[jj]                   #seed_indices defined at start
            xs=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][partochange].values      #x values of time course for one seed
            ys=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][var].values      #ind is an index
            yss.extend(ys)

            plt.plot(xs,ys,'-o',linewidth=lw);

        plt.xlabel(axlabels[str(partochange)],fontsize=fz)
        plt.ylabel(labels[var],fontsize=fz)
        

        #plt.xticks(np.logspace(-2,2,num=5), (r"0",r"2",r"4",r"6",r"8",r"10"),fontsize=fz)
        #plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)
        #if partochange=='seed_vol' :
        #    plt.xticks(np.logspace(-2,2,num=5),fontsize=fz)
        #else :
        #    plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)

        plt.yticks(np.arange(0,end_plotting_time,step=ticks_separation),fontsize=fz)

        # If there are just nans or non-positive in the ys, let's not do the log-axis transformation to prevent errors
        yss=np.array(yss)
        nan_array = np.isnan(yss)
        not_nan_array = ~ nan_array
        array2 = yss[not_nan_array]
        
        logscale=0

        if array2.size>0 :
            pos_array=array2>0
            array2_pos=array2[pos_array]
            if array2_pos.size>0 :
                logscale=1
                plt.xscale(scale);
                tosave=1
        else :
            tosave=0

        #plt.setp(ax.get_xticklabels(), fontsize=fz)
        #plt.xticks(np.arange(0,0.04,step=0.02))
        #ScalarFormatter(useOffset=True)
        rat = (ymax-ymin)/(xmax-xmin)

        if var=='cv_gt' and logscale==1:
            plt.yscale('log');
            ymax=1
            ymin=0.03
            #plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)
            #rat = (log10(ymax)-log10(ymin))/(xmax-xmin)
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))
            plt.tight_layout()
            
        if var=='fraction_germinated_seeds' :
            ymax=max(x[var])*1.1
            rat = (ymax-ymin)/(log10(xmax)-log10(xmin))
	    print('fr',rat)
            plt.yticks(np.arange(0,1.01,step=0.25),(r"0",r"25",r"50",r"75",r"100"),fontsize=fz)
            #plt.tight_layout()
            tosave=1
            gcf().set_size_inches(1.935,3.22)            
        if var=='mean_gt' :
            ymax=end_plotting_time*0.95
            plt.tight_layout()
            tosave=1
            
        if var=='mode_gt' and logscale==1 :
            ymin=1

            plt.yscale('log');
            ymax=end_plotting_time*0.95
            #rat = (log10(ymax)-log10(ymin))/(xmax-xmin)
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))
	    print('mode',rat)
            plt.tight_layout()
            
        #ax1.xaxis.grid(True, which='minor')
        
        plt.minorticks_off()
        plt.subplots_adjust(bottom=0.5)

        xmin=1

        if not(isnan(ymax)) :
            plt.axis([xmin,xmax,ymin,ymax])

        xlim(left=1)

        #ax1.set_aspect(0.618/rat)
        ax1.set_aspect(1.0/rat)
        
        if tosave==1 :
            fileout='_'+var
            fname=string_sim+par_string+"_dose"+fileout+"_bio.pdf"
            fname=var+'_vs_'+str(partochange)+'_'+str(parctt)+str(parctts[indx])+"_bio.pdf"

            fullfname=dirout+'/'+fname
            plt.savefig(fullfname, format=None)
        
        #plt.show()
        
        plt.close()
