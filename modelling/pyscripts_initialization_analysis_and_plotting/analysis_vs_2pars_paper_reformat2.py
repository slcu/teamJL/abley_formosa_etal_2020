#!/usr/bin/env python

# This python script plots organism derived results.
# Last modification: 1 June 2020.

variables_phds=['fraction_germinated_seeds']
variables_lphds=['cv_gt','mode_gt']
variables_norm_phds=['cv_gt','mode_gt']

# Importing packages###############
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['pdf.fonttype'] = 42
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
import matplotlib.colors as mcol

import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio
#import statistics

# extracted from vs_basal_aba_seed_vol30_deg_aba1_deg_ga1_bas_ga0.3_vmax_aba10_vmax_ga4_the_aba3.7_the_ga1.2_bas_targ0.3_deg_targ0.4_vmaxz_10_the_zaba6.5_deg2_target_6_the_zga6_basal_m_39_deg_m_0.1_bastime_ga_0.01_num_seeds4000_end_time1000_threshold2
# dose obs when bas_aba=1

#############################################
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots
fz=10  # font size for the different plots

# see below for fig size

dirini=os.getcwd()

with open("info.json", "r") as f:
    info= json.load(f)

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_parsexploration_path']
Results_dirout=info['strings']['Results_dirout']
num_realisations=info['initial_conds_pars']['num_realisations']
print num_realisations

#var_doses=info['strings']['var_doses']
string_sim=info['strings']['string_sim']
par1=info['varying_par']['par1']
par2=info['varying_par']['par2']
dimsweep=info['varying_par']['dimsweep']

var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']

get_bifdiag=info['plotting_pars']['get_bifdiag']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

par1sweep_from_run_script=info['parsweep'][par1]
par2sweep_from_run_script=info['parsweep'][par2]

main_path=info['strings']['main_path']

if 'CTL_main_parsexploration_path' in info['strings'] :
    CTL_dataname_out=info['strings']['CTL_main_parsexploration_path']

filedat=dataname_out+'/dose_obs.dat' 

if dimsweep==1 :
    colnames=['val_ic',par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size']
elif dimsweep==2 :
    colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']

ymin=min(par1sweep_from_run_script)
ymax=max(par1sweep_from_run_script)
xmin=min(par2sweep_from_run_script)
xmax=max(par2sweep_from_run_script)

x = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=colnames)
x=x[(x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)]

#x.cv_gt=x.cv_gt.replace(0, np.nan) # replacing CVs being 0 by nans.
x.cv_gt=x.cv_gt.replace(0, 0.01) # replacing CVs being 0 by nans.

pranges={'fraction_germinated_seeds':[0, 1],'cv_gt':[0, x['cv_gt'].max()],'mode_gt':[0, x['mode_gt'].max()],'mean_gt':[0, x['mean_gt'].max()]}
lpranges={'cv_gt':[-1.5, np.log10(x['cv_gt'].max())],'mode_gt':[-1, np.log10(x['mode_gt'].max())]}

lpranges={'cv_gt':[-2, 0],'mode_gt':[-1,3]}

ics=x['val_ic'].unique()

labels={'fraction_germinated_seeds':'Germination fraction','mean_gt':'Mean germination time (A.U.)','added_ga':'Exogenous GA','added_aba':'Exogenous ABA','cv_gt':'CV of germination time ','q2_gt':'Median germination time (A.U.)','cvq_gt': 'IQR/median of germination time','mode_gt':'Mode germination time (A.U.)','size':'Number of germinated seeds','theta_zga':'GA threshold for integrator','theta_zaba':'ABA threshold for integrator'}

# Labels for paper
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds','theta_ga':r'$\theta_{GA}$','theta_aba':r'$\theta_{ABA}$','basal_aba':'ABA Production','basal_ga':'GA Production','deg_aba':'ABA Degradation','deg_ga':'GA Degradation','theta_zga':r'$\theta_{I,GA}$','theta_zaba':r'$\theta_{I,ABA}$','seed_vol':'V','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','basal_target':'I Production'}
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median','cvq_gt': 'IQR/median ','mode_gt':'Mode','size':'Germinated seeds'}
axlabels={'theta_ga':'I threshold \n for GA inhibition ' r'($\theta_{GA,I})$','theta_aba':'I threshold \n for ABA activation ' r'$(\theta_{ABA,I})$','basal_aba':'ABA production ' r'($\beta_{ABA}$)','basal_ga':'GA production ' r'($\beta_{GA}$)','deg_aba':'ABA degradation ' r'($\nu_{ABA}$)','deg_ga':'GA degradation ' r'($\nu_{GA}$)','theta_zga': 'GA threshold for \n I degradation ' r'($\theta_{I,GA})$','theta_zaba': 'ABA threshold \n for I activation ' r'($\theta_{I,ABA})$','seed_vol':'1/(Noise intensity) ' r'($V$)','deg2_target':'GA-dependent \n I degradation ' r'($C_{I,GA}$)','deg_target': 'I degradation ' r'($\nu_{I})$','basal_target': 'I production ' r'($\beta_{I}$)','vmax_z':'ABA-dependent \n I activation' r'($C_{I,ABA}$)'}

filelabels={'fraction_germinated_seeds':'fr_germ_seeds','mean_gt':'mean_gt','q2_gt':'median_gt','cv_gt':'cv_gt','cvq_gt':'cvq_gt','mode_gt':'mode_gt','size':'gseedsnum'}

plot_threshold_bif=0

if get_bifdiag==1 :
    filedat=dataname_out+'/bif.dat' 
    x_bif = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=[par1,'numroots','branch','z0','g0','a0','bif','cumulativebif','underthreshold'])

    #print(x_bif)
    bifurcation_pts=x_bif[(x_bif['bif']==1)&(x_bif['branch']==0)]
    auxthresholdcross_pt=x_bif[(x_bif['underthreshold']==0)&(x_bif['branch']==0)&(x_bif['cumulativebif']==0)]

    if size(auxthresholdcross_pt)>0 :
        plot_threshold_bif=1
        thresholdcross_pt=auxthresholdcross_pt.iloc[0]

if 'CTL_dataname_out' in locals():
    CTL_filedat=CTL_dataname_out+'/dose_obs.dat' 
    if os.path.isdir('CTL_dataname_out') and os.path.isfile(CTL_filedat) :
        CTL_x = pd.read_csv(CTL_filedat,sep='\t',skip_blank_lines=True,names=colnames)
        CTL_x=CTL_x[(CTL_x[par2]>=xmin) & (CTL_x[par2]<=xmax) & (CTL_x[par1]>=ymin) & (CTL_x[par1]<=ymax)]

fiz=2.52
fiz=1.77
# Plotting figures
fig_size=(fiz,fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'

pylab.rcParams.update(params)

dirout = dataname_out+'/2d_plots'  #creating sub-directory
try:
    os.stat(dirout)
except:
    os.mkdir(dirout)


fz=8  # font size for the different plots

logscale=1

# IF parsweeps that have been simulated and are in the obs data file, uncomment the following:
#par1sweep=log10(x[par1].unique())
#par2sweep=log10(x[par2].unique())

par1sweep=log10(par1sweep_from_run_script)
par2sweep=log10(par2sweep_from_run_script)

yreps=size(par1sweep)

ymin=par1sweep.min()
ymax=par1sweep.max()
xmin=par2sweep.min()
xmax=par2sweep.max()

rat = (ymax-ymin)/(xmax-xmin)
logcol=0 
for ii in range(0,len(variables_phds)) :
    var_phd=variables_phds[ii]
    for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
        ind=ics[jj]   
        zs=x[(x.val_ic==ind)][var_phd].values
        #zs=x[(x.val_ic==ind) & (x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)][var_phd].values
        if logcol==1 :
            zs=np.log10(zs)
        z0ms=np.array_split(zs,yreps)
        data=np.array(z0ms).transpose()

        for zz in range(0,2,1) : # this is for producing one plot with cbar and another without
            if zz==0 :
                fiz=1.77
            else :
                fiz=2.52

            # Plotting figures
            fig_size=(fiz,fiz)   #for starting the plots
            params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
            rcParams['font.family'] = 'Arial'
            fig=plt.figure()
            ax = fig.add_subplot(111) 

            #fig, (ax,ax1) = plt.subplots(2)
            plt.subplots_adjust(left=0.40, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

            #ax.plt.imshow(data.transpose(), interpolation='bilinear', origin='lower',extent=(xmin,xmax,ymin,ymax))
            plt.imshow(data.transpose(), interpolation='bilinear', origin='lower',extent=(xmin,xmax,ymin,ymax),vmin=pranges[var_phd][0],vmax=pranges[var_phd][1])

            if logscale==1 :
                plt.yticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz) 
                plt.xticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz)   

            plt.axes().set_aspect(1.0/rat)

            #divider = make_axes_locatable(ax)
            #cax = divider.append_axes("right", size="5%", pad=0.05)
            if zz==1 :
                if var_phd=='fraction_germinated_seeds' :
                    #cb=plt.colorbar(ticks=[0, 0.25, 0.50,0.75,1], pad=0.15,shrink=0.55,orientation='horizontal')
                    #cb.ax.set_xticklabels(['0','25', '50','75','100'])
                    #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8]) 
                    cb=plt.colorbar(ticks=[0, 0.25, 0.50,0.75,1], pad=0.4,orientation='horizontal')
                    #cb=plt.colorbar(ax,cax = cbaxes,ticks=[0, 0.25, 0.50,0.75,1], orientation='horizontal')
                    cb.ax.set_xticklabels(['0','25', '50','75','100'])
                else :
                    #cb=plt.colorbar(shrink=0.55,aspect=20)
                    #cb=plt.colorbar(pad=0.15,shrink=0.55,orientation='horizontal')
                    cb=plt.colorbar(pad=0.40,orientation='horizontal')

                cb.ax.tick_params(labelsize=fz) 
                for t in cb.ax.get_yticklabels():
                      t.set_fontsize(fz)
            
            #print(filelabels)
            plt.title(labels[var_phd],fontsize=fz)        
            plt.ylabel(axlabels[par1],fontsize=fz)
            plt.xlabel(axlabels[par2],fontsize=fz)

            ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))

            string_vars=info['par_string_list'][par1]+'_vs_'+info['par_string_list'][par2]
            fname=filelabels[var_phd]+'_'+string_vars+"_ic"+str(ind)+".pdf"

            #fname=filelabels[var_phd]+'_'+string_vars+"_ic"+str(ind)+".pdf"
            if logcol==1 :
                fname='lc_'+fname

            if zz==1 :
                fname='cbar_'+fname

            minorLocator   = LogLocator(base=10.0,subs=np.log10(np.arange(1,10,1)))
            #plt.tight_layout()
            # saving the figure
            plt.savefig(dirout+'/'+fname)
            
            # close the figure
            plt.close()


logcol=1 

for ii in range(0,len(variables_lphds)) :
    for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
        ind=ics[jj]   
        var_phd=variables_lphds[ii]
        zs=x[(x.val_ic==ind)][var_phd].values
        #zs=x[(x.val_ic==ind) & (x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)][var_phd].values

        if logcol==1 :
            zs=np.log10(zs)
        z0ms=np.array_split(zs,yreps)
        data=np.array(z0ms).transpose()
        for zz in range(0,2,1) : # this is for producing one plot with cbar and another without
            if zz==0 :
                fiz=1.77
            else :
                fiz=2.52

            # Plotting figures
            fig_size=(fiz,fiz)   #for starting the plots
            params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
            rcParams['font.family'] = 'Arial'

            #ax.plt.i
            fig=plt.figure()
            ax = fig.add_subplot(111) 
            #plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
            plt.subplots_adjust(left=0.40, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

            #plt.imshow(data.transpose(), interpolation='bilinear', origin='lower',extent=(xmin,xmax,ymin,ymax))
            plt.imshow(data.transpose(), interpolation='bilinear', origin='lower',extent=(xmin,xmax,ymin,ymax),vmin=lpranges[var_phd][0],vmax=lpranges[var_phd][1])

            if logscale==1 :
                plt.yticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial') 
                plt.xticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial')   

                #plt.yticks(np.arange(-1,6,2), (r"$10^{-1}$",r"$10^{1}$",r"$10^{3}$",r"$10^{5}$"),fontsize=fz)  

            #plt.xticks(np.arange(0,6,1), (r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$"),fontsize=fz)  
            #plt.yticks(np.arange(0,6,1), (r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$"),fontsize=fz)  
            if zz==1 :                               
                if var_phd=='mode_gt' :
                #cb=plt.colorbar(shrink=0.56,aspect=20,ticks=[-3,-2, -1, 0,1,2,3])   
                    cb=plt.colorbar(ticks=[-1,1,3],pad=0.40,orientation='horizontal')           
                    cb.ax.set_xticklabels(["$10^{-1}$","$10^{1}$","$10^{3}$"])  # vertically oriented colorbar
                elif var_phd=='cv_gt' :
                #cb=plt.colorbar(shrink=0.54,aspect=20,ticks=[-2.5,-2,-1.5,-1,-0.5,0,0.5, 1, 1.5,2,2.5,3])           
                    cb=plt.colorbar(ticks=[-2,-1,0],pad=0.40,orientation='horizontal')           
                    cb.ax.set_xticklabels(["$10^{-2}$","$10^{-1}$","$10^{0}$"])  # vertically oriented colorbar

                #cb=plt.colorbar(shrink=0.54)
                cb.ax.tick_params(labelsize=fz) 
                for t in cb.ax.get_yticklabels():
                      t.set_fontsize(fz)
            
            plt.title(labels[var_phd],fontsize=fz)        

            plt.axes().set_aspect(1.0/rat)

            string_vars=info['par_string_list'][par1]+'_vs_'+info['par_string_list'][par2]
            #print(filelabels)
            plt.ylabel(axlabels[par1],fontsize=fz)
            plt.xlabel(axlabels[par2],fontsize=fz)

            ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))

            fname=filelabels[var_phd]+'_'+string_vars+"_ic"+str(ind)+".pdf"
            
            if logcol==1 :
                fname='lc_'+fname

            if zz==1 :
                fname='cbar_'+fname
            minorLocator   = LogLocator(base=10.0,subs=np.log10(np.arange(1,10,1)))
            
            #plt.tight_layout()
            # saving the figure
            plt.savefig(dirout+'/'+fname)
            
            # close the figure
            plt.close()


fiz=1.77

fig_size=(fiz,fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'

#par1sweep=log10(x[par1].unique())
#par2sweep=log10(x[par2].unique())
par1sweep=log10(par1sweep_from_run_script)
par2sweep=log10(par2sweep_from_run_script)

#lrat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))

ind=1


# those regions where target initial condition is above threshold, set it to 1.
zs=x[(x.val_ic==ind)]['ithtar'].values
z0ms=np.array_split(zs,yreps)
data=np.array(z0ms).transpose()

# those regions where final condition is below threshold, set it to 1.
zsb=x[(x.val_ic==ind)]['fthtar'].values
z0msb=np.array_split(zsb,yreps)
datab=np.array(z0msb).transpose()

zsf=x[(x.val_ic==ind)]['final_num_fp'].values
z0msf=np.array_split(zsf,yreps)
dataf=np.array(z0msf).transpose()

zsg=x[(x.val_ic==ind)]['fraction_germinated_seeds'].values
z0msg=np.array_split(zsg,yreps)
datag=np.array(z0msg).transpose()

zsm=x[(x.val_ic==ind)]['mode_gt'].values
z0msm=np.array_split(zsm,yreps)
datam=np.array(z0msm).transpose()

zsc=x[(x.val_ic==ind)]['cv_gt'].values
z0msc=np.array_split(zsc,yreps)
datacv=np.array(z0msc).transpose()

aux=np.array_split(np.log10(x[(x.val_ic==ind)][par1].values),yreps)
lxx=np.array(aux).transpose()
aux=np.array_split(np.log10(x[(x.val_ic==ind)][par2].values),yreps)
lyy=np.array(aux).transpose()

#final target condition is above threshold
colors=['b','w']
levels = [-1,0,1] 

if 'CTL_dataname_out' in locals():
    if os.path.isdir('CTL_dataname_out') and os.path.isfile(CTL_filedat) :
        zc=CTL_x[(CTL_x.val_ic==ind)]['fraction_germinated_seeds'].values
        z0mc=np.array_split(zc,yreps)
        datac=np.array(z0mc).transpose()

        levels = [0]  
        #CS2 = contour(lyy, lxx, datac, levels,colors='m',linewidths=1.0,linestyles='dashed')  # contour line 
        CS2 = contour(lyy, lxx, datac, levels,colors='m',linewidths=1.0)  # contour line 

        levels = [0.0001,1]  
        CS2 = contourf(lyy, lxx, datac, levels,colors='m',hatches=['//'],alpha=0.5)  # contour line 

        fig=plt.figure()
        ax = fig.add_subplot(111) 
        plt.subplots_adjust(left=0.40, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

        CS1bf = plt.contourf(lyy, lxx, datab, levels=levels,colors=colors,alpha=0.50)  # contour line 
        CS1b = plt.contour(lyy, lxx, datab, levels=levels,colors=colors,alpha=0.50)  # contour line 

        # marking regions with target ic below threshold
        levels = [-1,0,1]  
        colors=['m','w']

        hatches=['\\','']
        CS1 = plt.contourf(lyy, lxx, data, levels,colors=colors,hatches=hatches,alpha=0.50)  # contour line 
        CS1 = plt.contour(lyy, lxx, data, levels,colors=colors,alpha=0.5)  # contour line 

        levels = [0,1,3,5]
        colors=['w','k','g']
        #CS2 = plt.contour(lyy, lxx, dataf, levels,colors=colors)  # contour line 
        CS2f = plt.contourf(lyy, lxx, dataf, levels,colors=colors,alpha=0.50)  # contour line 

        #levels = [5]  
        #CS3 = contour(lyy, lxx, dataf, levels,colors='g',linewidths=1.0,linestyles='dashed')  # contour line 
        #levels = [4.9,6.1]  
        #CS3 = contourf(lyy, lxx, dataf, levels,colors='g',alpha=0.5)  # contour line 

        plt.ylabel(axlabels[par1],fontsize=fz)
        plt.xlabel(axlabels[par2],fontsize=fz)
        #plt.show()
        plt.yticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial') 
        plt.xticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial')   

        ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))
        plt.axes().set_aspect(1.0/rat)

        fname='phd_withCTL_'+string_vars+".pdf"

        minorLocator   = LogLocator(base=10.0,subs=np.log10(np.arange(1,10,1)))
        #plt.tight_layout()
        # saving the figure
        plt.savefig(dirout+'/'+fname)
        shutil.copyfile(dirout+'/'+fname,Results_dirout+'/phds_summaries/'+fname)

        plt.close()


fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.40, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

#final target condition is above threshold
colors=['b','w']
levels = [-1,0,1]  
CS1bf = plt.contourf(lyy, lxx, datab, levels=levels,colors=colors,alpha=0.50)  # contour line 
CS1b = plt.contour(lyy, lxx, datab, levels=levels,colors=colors,alpha=0.50)  # contour line 

# marking regions with target ic below threshold
levels = [-1,0,1]  
colors=['m','w']

hatches=['\\','']
CS1 = plt.contourf(lyy, lxx, data, levels,colors=colors,hatches=hatches,alpha=0.50)  # contour line 
CS1 = plt.contour(lyy, lxx, data, levels,colors=colors,alpha=0.5)  # contour line 

levels = [0,1,3,5]
colors=['w','k','g']
#CS2 = plt.contour(lyy, lxx, dataf, levels,colors=colors)  # contour line 
CS2f = plt.contourf(lyy, lxx, dataf, levels,colors=colors,alpha=0.50)  # contour line 

plt.ylabel(axlabels[par1],fontsize=fz)
plt.xlabel(axlabels[par2],fontsize=fz)
#plt.show()
plt.axes().set_aspect(1.0/rat)

plt.yticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial') 
plt.xticks(np.arange(-2,3,1), ("$10^{-2}$","$10^{-1}$","$10^{0}$","$10^{1}$","$10^{2}$"),fontsize=fz,fontname ='Arial')   

ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))

fname='phd_'+string_vars+".pdf"

minorLocator   = LogLocator(base=10.0,subs=np.log10(np.arange(1,10,1)))
#plt.tight_layout()
# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/phds_summaries/'+fname)

# marking regions with germination
colors=['w','y']
levels = [-1,0,1]
#hatches=['+']
CS1 = plt.contourf(lyy, lxx, datag, levels,colors=colors,alpha=0.50)  # contour line 


colors=['k']
levels = [0,0.9999] 

CS1 = plt.contour(lyy, lxx, datag, levels=levels ,colors=colors,alpha=0.50)  # contour line 

CS1 = plt.contour(lyy, lxx, datag, levels,colors=colors,alpha=0.50)  # contour line 

# marking mode
levels = [0,0.10001,40.0]
colors=['k','k']
linestyles=['dotted','dashed']
CS2 = plt.contour(lyy, lxx, datam, levels,colors=colors,linestyles=linestyles,alpha=0.50)  # contour line 

fname='phd_sims_mode_'+string_vars+".pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/phds_summaries/'+fname)

for coll in CS2.collections:
    coll.remove()

# marking mode
levels = [0,0.10,0.2,0.5]
colors=['r','r','r']
linestyles=['dotted','dashdot','dashed']
CS2 = plt.contour(lyy, lxx, datacv, levels,colors=colors,linestyles=linestyles,alpha=0.50)  # contour line 

fname='phd_sims_cv_'+string_vars+".pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/phds_summaries/'+fname)

plt.close()


