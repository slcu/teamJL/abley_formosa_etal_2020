#!/usr/bin/env python

import numpy as np
import os
import json
import pandas as pd

import scipy.optimize as opt

import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import pylab
from pylab import *

import eqs
from eqs import *

dirini=os.getcwd()  # storing the path in which this file is located
print(dirini)

fiz=6
fz=16
lw=1.5

with open("info.json", "r") as f:
    info= json.load(f)

#info= pickle.load(open('info.pkl'))    #reading parameters

#ga_ic=info['initial_conds_pars']['ga_ic']; # 
#aba_ic=info['initial_conds_pars']['aba_ic']; #
#target_ic=info['initial_conds_pars']['target_ic']; # 
thresholds_for_germination=info['initial_conds_pars']['thresholds_for_germination']; # 
num_seeds=info['initial_conds_pars']['num_seeds']

deg_aba=info['model_pars']['deg_aba']; # 
deg_ga=info['model_pars']['deg_ga']; # 
basal_aba=info['model_pars']['basal_aba']; # 
basal_ga=info['model_pars']['basal_ga']; # 
bastime_ga=info['model_pars']['bastime_ga'];

basal_m=info['model_pars']['basal_m'];
deg_m=info['model_pars']['deg_m'];

vmax_aba=info['model_pars']['vmax_aba']; # 
vmax_ga=info['model_pars']['vmax_ga']; # 
seed_vol=info['model_pars']['seed_vol']; # 

theta_aba=info['model_pars']['theta_aba']; # 
theta_ga=info['model_pars']['theta_ga'] # 
hhh=info['model_pars']['hhh'] #

basal_target=info['model_pars']['basal_target']; # 
deg_target=info['model_pars']['deg_target']; # 
vmax_z=info['model_pars']['vmax_z']; #
theta_zaba=info['model_pars']['theta_zaba']; #
delta_1aba=info['model_pars']['delta_1aba']; #
h_1=info['model_pars']['h_1'] #
deg2_target=info['model_pars']['deg2_target']; #
theta_zga=info['model_pars']['theta_zga']; #
delta_2ga=info['model_pars']['delta_2ga']; #
h_2=info['model_pars']['h_2'] #

var_doses=info['strings']['var_doses']


labels={'added_ga':'Exogenous GA','added_aba':'Exogenous ABA'}

numpointsdiag=600
#added_ga=info['doses_pars']['added_ga']
#added_aba=info['doses_pars']['added_aba']

if var_doses=='added_ga' :
	added_abas=[0]
	added_gas=np.logspace(-1, 2, num=numpointsdiag)
	added_gas=np.linspace(0, 10, num=numpointsdiag)

else :
	added_abas=np.logspace(-1, 2, num=numpointsdiag)
	added_gas=[0]
	added_abas=np.linspace(0, 10, num=numpointsdiag)


numpoints=1000

list_X=np.logspace(-2, 5, num=numpoints)

#nullcl=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(list_X,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-list_X

def fnullcl(x) :
	return zfunc1(afunct(x,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(x,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-x

nullcl=fnullcl(list_X)
bisect=np.zeros(numpoints-1)

for i in range(0,numpoints-1) :
	bisect[i]=nullcl[i]*nullcl[i+1]

indroots=np.where(bisect < 0)[0]
numroots=len(indroots)

#bisect[indroots[i]]
zs=[]
for i in range(0,numroots,1) :
	y0=nullcl[indroots[i]]
	y1=nullcl[indroots[i]+1]

	x0=list_X[indroots[i]]
	x1=list_X[indroots[i]+1]

	z0 = opt.brentq(fnullcl,x0,x1)
	zs.append(z0)

zi=max(zs)
target_icl=min(zs)

gi=gfunct(zi,basal_ga,vmax_ga,deg_ga,theta_ga,hhh)
ai=afunct(zi,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)

# adding these to the json dictionary
info['initial_conds_pars']['ga_ic']=gi # 
info['initial_conds_pars']['aba_ic']=ai #
info['initial_conds_pars']['target_ic']=zi; 
print(str(numroots)+' computed roots ! Ic is g_ic='+str(gi)+' aba_ic='+str(ai))

newinfo={'analysis':{'start_num_fp' : numroots,'target_icl': target_icl}}
info.update(newinfo)



##

def fnullcl_pert_general(x,added_aba,added_ga,ga_production) :
    return zfunc1(afunct(x,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)+added_aba,gfunct(x,ga_production,vmax_ga,deg_ga,theta_ga,hhh)+added_ga,basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-x
	#return zfunc1(afunct(x,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(x,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-x

def fnullcl_pert_t0(x,added_aba,added_ga) :
    return fnullcl_pert_general(x,added_aba,added_ga,basal_ga)

def fnullcl_pert_t1(x,added_aba,added_ga) :
    return fnullcl_pert_general(x,added_aba,added_ga,basal_ga+bastime_ga*basal_m/deg_m)


#nuc=fnullcl_pert_general(list_X,added_aba,added_ga,basal_ga)

zs=[]

cumulativebif=0
for k in range(0,len(added_abas),1) :
	added_aba=added_abas[k]
	for j in range(0,len(added_gas),1) :
		added_ga=added_gas[j]

		def fnullcl_pert_t1x(x) :
		    return fnullcl_pert_t1(x,added_aba,added_ga)

		nullcl1=fnullcl_pert_t1(list_X,added_aba,added_ga)
		bisect=np.zeros(numpoints-1)

		for i in range(0,numpoints-1) :
			bisect[i]=nullcl1[i]*nullcl1[i+1]

		indroots=np.where(bisect < 0)[0]
		numroots=len(indroots)
		#bisect[indroots[i]]
		
		if (k+j)>0 :
			if auxnumroots==numroots :
				bif=0
			else :
				bif=1
		else :
			bif=0
			
		cumulativebif=cumulativebif+bif

		for i in range(0,numroots,1) :
			y0=nullcl1[indroots[i]]
			y1=nullcl1[indroots[i]+1]

			x0=list_X[indroots[i]]
			x1=list_X[indroots[i]+1]

			z0 = opt.brentq(fnullcl_pert_t1x,x0,x1)
			#print(str(z0))

			if z0<thresholds_for_germination :
				underthreshold=1
			else :
				underthreshold=0 

			g0=gfunct(z0,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)
			a0=afunct(z0,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)

			zs.append([added_aba, added_ga,numroots,i,z0,g0,a0,bif,cumulativebif,underthreshold])

		auxnumroots=numroots

x_bif=pd.DataFrame(zs,columns=['added_aba','added_ga','numroots','branch','z0','g0','a0','bif','cumulativebif','underthreshold'])
#print(x_bif)
#zi=max(zs)
#gi=gfunct(zi,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)
#ai=afunct(zi,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)

x_bif.to_csv(path_or_buf='bif.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

#bifurcation_pts=x_bif[(x_bif['bif']==1)&(x_bif['branch']==0)]
#thresholdcross_pt=x_bif[(x_bif['underthreshold']==1)&(x_bif['branch']==0)].iloc[0]

#newinfo={'analysis':{'bifurcation_pts' : bifurcation_pts}}
#info.update(newinfo)

# ,'thresholdcross_pt' : thresholdcross_pt


with open('info.json','w') as f:
    json.dump(info, f)


ymin=0.009
ymax=12

xmin=max(min(added_gas),min(added_abas)) # put the minimum added aba or ga that is not 0.
xmax=max(max(added_gas),max(added_abas))


fig_size=(0.7*fiz,0.7*fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size}
pylab.rcParams.update(params)


fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
                                       
plt.xlabel(labels[var_doses],verticalalignment='top',fontsize=fz)

#plt.ylabel(r"[$RGL2$]",fontsize=fz)
plt.ylabel(r"[$I$]",fontsize=fz)

if var_doses=='added_ga' :
	xs=x_bif['added_ga']
	xs_thres=added_gas
else :
	xs=x_bif['added_aba']
	xs_thres=added_abas

#plt.loglog(xs,x_bif['z0'],'o',linewidth=lw,color = '0.75',markersize=4)
plt.plot(xs,x_bif['z0'],'o',linewidth=lw,color = '0.75',markersize=4)

threshold_in_phasediagram=np.zeros(numpointsdiag)+thresholds_for_germination
#plt.loglog(xs_thres,threshold_in_phasediagram,'-.',linewidth=lw,color = 'b')
plt.plot(xs_thres,threshold_in_phasediagram,'-.',linewidth=lw,color = 'b')

plt.axis([xmin,xmax,ymin,ymax])

fname="bifdiag.pdf"
# saving the figure
plt.savefig(fname)

#

ymin=0.5
ymax=12

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
plt.xlabel(labels[var_doses],verticalalignment='top',fontsize=fz)

plt.ylabel(r"[$GA$]",fontsize=fz)

if var_doses=='added_ga' :
	xs=x_bif['added_ga']
	xs_thres=added_gas
else :
	xs=x_bif['added_aba']
	xs_thres=added_abas

#plt.loglog(xs,x_bif['g0'],'o',linewidth=lw,color = '0.75',markersize=4)
plt.plot(xs,x_bif['g0'],'o',linewidth=lw,color = '0.75',markersize=4)

plt.axis([xmin,xmax,ymin,ymax])

fname="bifdiag_g.pdf"
# saving the figure
plt.savefig(fname)

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
plt.xlabel(labels[var_doses],verticalalignment='top',fontsize=fz)

plt.ylabel(r"[$ABA$]",fontsize=fz)

if var_doses=='added_ga' :
	xs=x_bif['added_ga']
	xs_thres=added_gas
else :
	xs=x_bif['added_aba']
	xs_thres=added_abas

#plt.loglog(xs,x_bif['a0'],'o',linewidth=lw,color = '0.75',markersize=4)
plt.plot(xs,x_bif['a0'],'o',linewidth=lw,color = '0.75',markersize=4)

plt.axis([xmin,xmax,ymin,ymax])

fname="bifdiag_a.pdf"
# saving the figure
plt.savefig(fname)
