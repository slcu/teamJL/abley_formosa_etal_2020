#' rank normalisation
#'
#' see https://statmodeling.stat.columbia.edu/2015/07/13/dont-do-the-wilcoxon/
#'
#' @param x a numeric vector
#'
#' @return a numeric vector of transformed values
rank_norm <- function(x){
  # replace data by ranks - ties are taken as their integer part
  ranks <- rank(x, na.last = "keep")

  # sequence of odd numbers divided by 2x sample size
  odds <- seq(1, 2*length(x) - 1, by = 2)/(2*length(x))

  # inverse normal
  ys <- qnorm(odds[ranks])

  return(ys)
}
