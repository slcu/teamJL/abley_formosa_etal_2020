#!/usr/bin/env python

# This python script plots the statistics of several 2D parameters scans.

variables_phds=['fraction_germinated_seeds','cv_gt','mean_gt','mode_gt']
variables_lphds=['cv_gt','mode_gt']
variables_norm_phds=['cv_gt','mode_gt']

variables_to_plot=['fraction_germinated_seeds','mean_gt','q2_gt','cv_gt','size','mode_gt','cvq_gt']
normalised_variables_to_plot=['mode_gt','cv_gt']

# Importing packages###############
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['pdf.fonttype'] = 42
#matplotlib.rcParams['axes.unicode_minus'] = False
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
#from matplotlib import rc, font_manager
import matplotlib.colors as mcol

import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio
#import statistics

# extracted from vs_basal_aba_seed_vol30_deg_aba1_deg_ga1_bas_ga0.3_vmax_aba10_vmax_ga4_the_aba3.7_the_ga1.2_bas_targ0.3_deg_targ0.4_vmaxz_10_the_zaba6.5_deg2_target_6_the_zga6_basal_m_39_deg_m_0.1_bastime_ga_0.01_num_seeds4000_end_time1000_threshold2
# dose obs when bas_aba=1
modemean=np.mean([37.2,43.6,42.2,40.0,39.4])
cvmean=np.mean([0.2638589249952464,0.27038861272721265,0.26751566005861954,0.26461334210629944,0.2698380582720033])

#############################################
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots
fz=10  # font size for the different plots
fz=10  # font size for the different plots

# see below for fig size

dirini=os.getcwd()

with open("info.json", "r") as f:
    info= json.load(f)

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_parsexploration_path']
Results_dirout=info['strings']['Results_dirout']
num_realisations=info['initial_conds_pars']['num_realisations']
print num_realisations

#var_doses=info['strings']['var_doses']
string_sim=info['strings']['string_sim']
par1=info['varying_par']['par1']
par2=info['varying_par']['par2']
dimsweep=info['varying_par']['dimsweep']

var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']

get_bifdiag=info['plotting_pars']['get_bifdiag']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

par1sweep_from_run_script=info['parsweep'][par1]
par2sweep_from_run_script=info['parsweep'][par2]

main_path=info['strings']['main_path']

colnames=['mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region']
phd_stats = pd.read_csv(Results_dirout+'/'+'phds_stats.dat',sep='\t',skip_blank_lines=True,names=colnames)

import seaborn as sns

axlabels={'mdMode':'Median(Mode) (A.U.)','mnMode':'Mean(Mode) (A.U.)','cvMode':'CV(Mode)','mdCV':'Median(CV)','mnCV':'Mean(CV)','cvCV':'CV(CV)'}

filelabels={'mdMode':'medianMode','mnMode':'meanMode','cvMode':'cvMode','mdCV':'medianCV','mnCV':'meanCV','cvCV':'cvCV'}

fiz=2.5
# Plotting figures
fig_size=(fiz,fiz) 
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'
pylab.rcParams.update(params)

for i in range(0,len(colnames)-1,1) :
    sample=colnames[i]


    fig=plt.figure()
    ax = fig.add_subplot(111) 
    plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

    #ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns)
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats,color='b')

    ax.set(ylim=(0,2))

    if i<2 :
        ax.set_yscale("log")
        ax.set(ylim=(0.1, 1000))

    plt.ylabel(axlabels[sample],fontsize=fz)
    plt.xlabel('Regime',fontsize=fz)
    plt.xticks([0,1,2],('Monostable G \n (white region)','Bistable','Monostable NG\n (blue region)'),fontsize=fz-3)

    ax.set(xlim=(-1, 3))

    #plt.show()

    fname='swplot_phds_'+filelabels[sample]+".pdf"

    # saving the figure
    plt.savefig(Results_dirout+'/sims_summaries_stats/'+fname)
    #shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
    #plt.close()


colnames_ics=['ics','mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region']
phd_stats_ics = pd.read_csv(Results_dirout+'/'+'phds_stats_ics.dat',sep='\t',skip_blank_lines=True,names=colnames_ics)

for i in range(0,len(colnames)-1,1) :
    sample=colnames[i]


    fig=plt.figure()
    ax = fig.add_subplot(111) 
    plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

    #ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns)
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==1],color='b')
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==2],color='orange')
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==3],color='green')

    ax.set(ylim=(0,2))

    if i<2 :
        ax.set_yscale("log")
        ax.set(ylim=(0.1, 1000))

    plt.ylabel(axlabels[sample],fontsize=fz)
    plt.xlabel('Regime',fontsize=fz)
    plt.xticks([0,1,2],('Monostable G \n (white region)','Bistable','Monostable NG\n (blue region)'),fontsize=fz-3)

    ax.set(xlim=(-1, 3))

    #plt.show()

    fname='swplot_phds_ics_withblue'+filelabels[sample]+".pdf"

    # saving the figure
    plt.savefig(Results_dirout+'/sims_summaries_stats/'+fname)


    sample=colnames[i]



    fig=plt.figure()
    ax = fig.add_subplot(111) 
    plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

    #ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns)
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==1],color='b')
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==2],color='orange')
    ax=sns.swarmplot(x="region",y=sample,data=phd_stats_ics[phd_stats_ics['ics']==3],color='green')

    ax.set(ylim=(0,1))
    if i==2 :
        ax.set(ylim=(0,2))
    if i<2 :
        ax.set_yscale("log")
        ax.set(ylim=(0.1, 1000))

    plt.ylabel(axlabels[sample],fontsize=fz)
    plt.xlabel('Regime',fontsize=fz)
    plt.xticks([0,1],('Monostable \n (white region)','Bistable'),fontsize=fz-2)

    ax.set(xlim=(-0.8, 1.8))

    #plt.show()

    fname='swplot_phds_ics_'+filelabels[sample]+".pdf"

    # saving the figure
    plt.savefig(Results_dirout+'/sims_summaries_stats/'+fname)
