#!/usr/bin/env python

# This python script executes organism performs doses responses simulations of the ABA/GA model and its subsequent analysis.

#########################################################################################################################

# Parameters and strings to modify by the user

# Strings to modify

rootsim='../../../Organism/bin/simulator'			 # path in which the organism executable simulator is located
pyscripts='pyscripts_initialization_analysis_and_plotting' # scripts subfolder name


# Most important general script parameters

sims=1              # sims=1 runs organism simulation, sims=0 does not run organism. NOTE: when sims=0, findroots is not executed, so ic are set to 0 in the json file.
plot_doses_analysis=0 # set it to 1 to plot the doses analysis, otherwise leave it to 0.

findroots=1         # set it to 0 to find the fixed points of the dynamics. Otherwise, set to 0 ( Note: if set to 0, if simulations are run, note that initial conditions will be set to 0)
get_bifdiag=1       # set it to 1 to get the bifurcation diagram dependent on the exogenous concentrations; otherwise set it to 0. Note, this needs to be 1 in case the doses analysis script is run.
plot_bif_diag=1     # set it to 1 to plot the bifurcation diagram.

get_obs=1           # set it to 1 to extract the main outcomes and export it to obs.dat files, otherwise set it to 0 (setting it to 0 will prevent doing the dose response analysis and plots, unless these obs.dat files have been created beforehand)

do_single_simulation_analysis=1 # set it to 1 to analyse single simulations, and allow doing plottings of it.
plotting_individual_simulations_figs=1 # set it to 1 to plot the figures for each single simulation

do_doses_analyses=1     # set it to 1 to plot the dose responses, otherwise set it to 0.
plot_theolines=0        # set to 1 to plot theoretical lines in timecourse of a mean theoretical exponential decay. Leave it to 0 for now.

seed_vol=30         # V parameter, inversely proportional to dynamic noise amplitude

num_seeds=4000 # number of seeds
end_time=1000 # final simulation time


# if doses analysis is set, then we need to do bifurcation diagram
if do_doses_analyses==1 :
    get_bifdiag=1
#######

# Simulation parameters

# Model parameters

list_exogenous_gas=[0,0.1,0.25,0.5,1,2,2.5,5,10]
list_exogenous_abas=[0]

num_vars=8  # number of variables - important for the generation of the init file

bastime_ga=0.01    # production rate in the GA equation for the modulator (Z factor in the paper). Setting it to 0 would mean there is no rise of GA biosynthesis after sowing (CTL simulations)

# basal productions
basal_aba=1.0    # basal production for ABA
basal_ga=0.3     # basal production for GA
basal_target=0.3 # basal production for the Integrator
basal_m=39       # basal production for the Modulator (called Z in the paper)

# degradations
deg_aba=1  # ABA degradation
deg_ga=1   # GA degradation
deg_m=0.1  # degradation rate for modulator (called Z in the paper)
deg_target=0.4 # Integrator degradation

# thresholds in the hill functions for ABA, GA and Integrator
theta_aba=3.7    # 'threshold' in hill function for activation of ABA by Integrator
theta_ga=1.2     # 'threshold' in hill function for inhibition of GA by Integrator

theta_zaba=6.5 # 'threshold' in hill function for biosynthesis of Integrator by ABA
theta_zga=6     # 'threshold' in hill function for degradation of Integrator mediated by GA


# coefficients for the regulatory hill functions
deg2_target=6 # coefficient in the GA-mediated degradation of Integrator
vmax_aba=10   # coefficient of Hill term in ABA equation
vmax_ga=4   # coefficient of Hill term in GA equation
vmax_z=10   # coefficient of Hill term in Integrator equation


# Hill exponents
hhh=4       # cooperativity for ABA and GA Hill functions
h_1=hhh      # Activator hill exponent for promotion of Integrator accumulation by ABA
h_2=hhh      # Hill exponent for promotion of GA-induced degradation

thresholds_for_germination=2

# static noise parameters (to omit in these simulations, set them to 0)
delta_1aba=0
delta_2ga=0

# Initial conditions set to zero here, but afterwards in the script they are assigned to the corresponding fixed points.
ga_ic=0
aba_ic=0
target_ic=0

list_ics=[1,2,3,4,5]

num_realisations=len(list_ics)

# More general script parameters. Set them to 1 to perform its associated task, otherwise, set them to 0.

generate_ic=1   # generate_ic=1 generate new initial conditions from the fixed points of the dynamics

if sims==0 :
    generate_ic=0

plot_histos=1 			 # plot_histos=1 plots initial and final histograms of the simulated variables, plot_histos=0 does not plot it
plot_timecourses=0  	 # plot_timecourses=1 plots time courses of the simulated variables, plot_timecourses=0 does not plot it
plot_phasediag=1  		 # plot_phasediag=1 plots the nullclines of the dynamical system with and without trajectiories
plot_timecourses_productionga=0

plot_target_vs_gaaba=0   # plot_target_vs_gaaba=1 plots the target versus the aba/ga ratio
plot_timecourse_germination_fraction=0

aspect_histos=0.45


# Other strings to modify

solver='hi'			  			 # solver file (eg., rk, hi and gi) that is going to be used,

model='zabaga_doses_v0'		# model file name without the '.model' extension
script_analysis='analysis_zabaga_results_paper.py'
script_analysis_doses='analysis_dose_response_paper-layout_change.py'

string_sim='2020-10-21_' # Substring of the subfolder where the results are going to be stored

# Lines to automatically determine the exogenous exploration we are doing, depending the length of the ABA and GA exogenous concentrations lists.
num_exoabas=len(list_exogenous_abas)
num_exogas=len(list_exogenous_gas)

if num_exoabas>1 :
    var_doses='added_aba'
    end_time=3000
    ticks_separation=500
    end_time=1000
    end_plotting_time=end_time
    ticks_separation=250
    num_print_times=int(end_time/1000.0)+1
elif num_exogas>1 :
    var_doses='added_ga'
    end_time=250
    end_time=1000
    end_plotting_time=140
    ticks_separation=40
    num_print_times=int(end_time/1000.0)+1

if num_exogas>1 :
    dose_string='ga'
else :
    dose_string='aba'

if (sims==1 and findroots==1) :
    get_bifdiag=1

if (num_exogas==1 and num_exoabas==1) :
    var_doses='added_aba'
    dose_string='sim'
    end_plotting_time=end_time
    ticks_separation=200
    end_time=1000
    num_print_times=int(end_time/20.0)+1
    do_doses_analyses=0
    get_bifdiag=1

    end_plotting_time=500
    ticks_separation=100

##############################################################################################################
##### Importing packages #####################################################################################

# Importing packages

import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab
import os
import shutil
import subprocess
import pickle
import json
import pandas as pd

##############################################################################################################


listpar=[5.8,7] # theta_zaba for emulating the low variability and high variability lines

dirini=os.getcwd()  # storing the path in which the run_sepals.py file is located

for ky in range(0,len(listpar)) :
    os.chdir(dirini) # going back to the abaga directory

    theta_zaba=listpar[ky]

    #  Note: for simplicity, I took out from the dataname string for now the following : +'_hhh_'+str(hhh)+'_h_1_'+str(h_1)+'_h_2_'+str(h_2)    ;  +'_del_1aba'+str(delta_1aba) ;    +'_del_2ga'+str(delta_2ga)
    dataname=dose_string+"_doses_"+'seed_vol'+str(seed_vol)+'_deg_aba'+str(deg_aba)+'_deg_ga'+str(deg_ga)+'_bas_aba'+str(basal_aba)+'_bas_ga'+str(basal_ga)+'_vmax_aba'+str(vmax_aba)+'_vmax_ga'+str(vmax_ga)+'_the_aba'+str(theta_aba)+'_the_ga'+str(theta_ga)+'_bas_targ'+str(basal_target)+'_deg_targ'+str(deg_target)+'_vmaxz_'+str(vmax_z)+'_the_zaba'+str(theta_zaba)+'_deg2_target_'+str(deg2_target)+'_the_zga'+str(theta_zga)+'_basal_m_'+str(basal_m)+'_deg_m_'+str(deg_m)+'_bastime_ga_'+str(bastime_ga)+'_num_seeds'+str(num_seeds)+'_end_time'+str(end_time)+'_threshold'+str(thresholds_for_germination)
    dataname_out='Results_doses/'+dataname
    print(dataname)

    dataname_init='aba_ic_'+str(round(aba_ic,3))+'_ga_ic_'+str(round(ga_ic,3))+'_target_ic_'+str(round(target_ic,2))+'_thgerm_'+str(thresholds_for_germination)+'_num_seeds_'+str(num_seeds)+'_num_vars_'+str(num_vars) #

    initfilename=dataname_init;

    fullmodelfile='ModelFiles/'+model+'.model'
    fullsolverfile='SolverFiles/'+solver

    # Model parameters

    # Generating a dictionary of parameters and storing them in a file
    info={'initial_conds_pars':{'ga_ic' : ga_ic, 'aba_ic' : aba_ic, 'target_ic' : target_ic, 'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times,'start_ic_seed' : list_ics[0]}, \
    'model_pars': {'num_vars' : num_vars, 'deg_aba' : deg_aba, 'deg_ga' : deg_ga, 'deg_m':deg_m,'basal_aba' : basal_aba, 'basal_ga' : basal_ga, 'basal_m':basal_m, 'bastime_ga':bastime_ga,'vmax_aba' : vmax_aba, 'vmax_ga' : vmax_ga, 'theta_aba' : theta_aba, 'theta_ga' : theta_ga , 'deg_target' : deg_target, 'basal_target' : basal_target, 'vmax_z' : vmax_z, 'deg2_target' : deg2_target, 'theta_zaba': theta_zaba, 'theta_zga': theta_zga, 'delta_1aba': delta_1aba, 'delta_2ga': delta_2ga, 'hhh': hhh, 'h_1': h_1, 'h_2': h_2,'seed_vol':seed_vol}, \
    'plotting_pars': {'plotting_individual_simulations_figs': plotting_individual_simulations_figs,'plot_bif_diag':plot_bif_diag,'plot_timecourses_productionga':plot_timecourses_productionga,'plot_histos' : plot_histos, 'aspect_histos' : aspect_histos,'plot_timecourses' : plot_timecourses,'plot_phasediag' : plot_phasediag, 'plot_target_vs_gaaba' : plot_target_vs_gaaba,'plot_timecourse_germination_fraction': plot_timecourse_germination_fraction,'plot_theolines':plot_theolines,'end_plotting_time':end_plotting_time,'ticks_separation':ticks_separation,'get_obs': get_obs}, \
    'strings' : {'main_path' : dirini ,'main_doses_path' : dataname_out, 'dose_string': dose_string, 'var_doses' : var_doses,'string_sim': string_sim,'init_filename':initfilename}}

    try:
        os.stat('Results_doses')
    except:
        os.mkdir('Results_doses') # creating the directory

    try:
        os.stat(dataname_out)
    except:
        os.mkdir(dataname_out) # creating the directory

    if get_bifdiag==1 :
        # creating a json to pass parameters to the findroots script
        with open('info.json','w') as f:
            json.dump(info, f)

        print('executing findroots')
        command0="./"+pyscripts+"/findroots.py"
        subprocess.call(command0,shell=True)
        shutil.move('bif.dat',dataname_out+'/bif.dat')

        diroutdoses= dataname_out+'/dose_responses_plots'  #creating sub-directory
        try:
            os.stat(diroutdoses)
        except:
            os.mkdir(diroutdoses)

        if plot_bif_diag==1 :
            # moving output to the results subfolder

            shutil.move('bifdiag.pdf',diroutdoses+'/bifdiag.pdf')                      # moving output to the results subfolder
            shutil.move('bifdiag_a.pdf',diroutdoses+'/bifdiag_a.pdf')                      # moving output to the results subfolder
            shutil.move('bifdiag_g.pdf',diroutdoses+'/bifdiag_g.pdf')                      # moving output to the results subfolder

        # reading back the results, to get the fixed points
        with open("info.json", "r") as f:
            info= json.load(f)

    # This line is to make sure I can recover the used python script of a runned simulation
    if (sims==1) :
        shutil.copyfile('run_zabaga_doses_ga_v2_x-label_reformat.py',dataname_out+'/run_zabaga_doses_ga_v2_x-label_reformat.py')

    for index_abas in range(0,len(list_exogenous_abas),1) :
        added_aba=list_exogenous_abas[index_abas]
        for index_gas in range(0,len(list_exogenous_gas),1) :
            added_ga=list_exogenous_gas[index_gas]
            print("added ABA="+str(added_aba)+", added GA="+str(added_ga))
            os.chdir(dirini) # going back to the abaga directory

            newinfo={'doses_pars':{'added_ga' : added_ga, 'added_aba' : added_aba}}
            info.update(newinfo)
            with open('info.json','w') as f:
                json.dump(info, f)

    		# creating the output folder in case it does not exist

            if (generate_ic==1) and ((index_gas+index_abas)==0) :
                subprocess.call("./"+pyscripts+"/initfile_generator_organism_nonspatial_zabaga.py",shell=True)

            for ics in range(0,len(list_ics),1) :
                os.chdir(dirini)
                val_ic=list_ics[ics]
                string_pars='added_ga'+str(round(added_ga,3))+'_added_aba'+str(round(added_aba,3))+'_ic_'+str(val_ic)


                subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                dirout = dataname_out+'/'+subfolder_out
                print(subfolder_out)
                try:
                    os.stat(dirout)
                except:
                    os.mkdir(dirout)

                with open(dirout+'/info.json','w') as f:
                    json.dump(info, f)

                if (sims==1) :

                    initfile='InitFiles/'+dataname_init+'/'+initfilename+'_ic_'+str(val_ic)+'.init'
                    shutil.copyfile(initfile,dataname_out+'/'+initfilename+'_ic_'+str(val_ic)+'.init')	# moving data init file into the output folder

                    outfile=model+'_'+solver+'_num_out'

                    # introducing parameters into the model file
                    command0="sed -e 's/degaba/"+str(deg_aba)+"/g' -e 's/degga/"+str(deg_ga)+"/g' -e 's/degm/"+str(deg_m)+"/g' -e 's/basalaba/"+str(basal_aba)+"/g' -e 's/basalga/"+str(basal_ga)+"/g' -e 's/bastimega/"+str(bastime_ga)+"/g' -e 's/basalm/"+str(basal_m)+"/g' -e 's/vmaxaba/"+str(vmax_aba)+"/g' -e 's/vmaxga/"+str(vmax_ga)+"/g' -e 's/thetaga/"+str(theta_ga)+"/g' -e 's/thetaaba/"+str(theta_aba)+"/g' -e 's/degtarget/"+str(deg_target)+"/g' -e 's/basaltarget/"+str(basal_target)+"/g' -e 's/vmaxz/"+str(vmax_z)+"/g' -e 's/deg2target/"+str(deg2_target)+"/g' -e 's/thetazaba/"+str(theta_zaba)+"/g' -e 's/thetazga/"+str(theta_zga)+"/g' -e 's/delta1aba/"+str(delta_1aba)+"/g' -e 's/delta2ga/"+str(delta_2ga)+"/g' -e 's/hhh/"+str(hhh)+"/g' -e 's/h1/"+str(h_1)+"/g' -e 's/h2/"+str(h_2)+ "/g' -e 's/addedga/"+str(added_ga)+"/g' -e 's/addedaba/"+str(added_aba)+"/g' ModelFiles/pre_"+model+".model >  ModelFiles/"+model+".model"
                    subprocess.call(command0,shell=True)

                    # introducing parameters into the solver file
                    command0="sed -e 's/seedvol/"+str(seed_vol)+"/g' -e 's/endtime/"+str(end_time)+"/g' -e 's/numprinttimes/"+str(num_print_times)+"/g' SolverFiles/pre_"+solver+" >  SolverFiles/"+solver
                    subprocess.call(command0,shell=True)

                    initfilename_extended=initfilename+'_ic_'+str(val_ic)+'.init'
                    fullinitfile='InitFiles/'+dataname_init+'/'+initfilename_extended
                    outfile='num_out'

                    cmd=rootsim+' '+fullmodelfile+' '+fullinitfile+' '+fullsolverfile #+'  > '+dirout+'/'+outfile  # command line for running organism
                    print('Running '+cmd)
                    subprocess.call(cmd,shell=True)

                    shutil.copyfile(fullmodelfile,dirout+'/'+model+'.model')		# copying model file
                    shutil.move('organism.gdata',dirout+'/organism.gdata')					    # moving output to the results subfolder
                    shutil.copyfile(initfile,dirout+'/'+initfilename_extended)				# copying init file to the results subfolder
                    shutil.copyfile(fullsolverfile,dirout+'/'+solver)				# copying integrator file to the results subfolder

                    # plotting loop

                if do_single_simulation_analysis==1 :
                    outfile=model+'_'+solver+'_num_out'
                    subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                    dirout = dataname_out+'/'+subfolder_out
                    shutil.copy2(pyscripts+'/'+script_analysis,dirout+'/'+script_analysis)
                    shutil.copy2(pyscripts+'/eqs.py',dirout+'/eqs.py')

                    os.chdir(dirini+'/'+dirout)
                    subprocess.call("./"+script_analysis,shell=True)	# necessary for creating the dictionaries

    if do_doses_analyses==1 :
        print("Doing doses analyses")
        # putting together all the simulation results into a panda and exporting this to the dose_obs file
        ind=0
        for index_abas in range(0,len(list_exogenous_abas),1) :
            added_aba=list_exogenous_abas[index_abas]
            for index_gas in range(0,len(list_exogenous_gas),1) :
                added_ga=list_exogenous_gas[index_gas]
                for ics in range(0,len(list_ics),1) :
                    os.chdir(dirini)
                    val_ic=list_ics[ics]
                    string_pars='added_ga'+str(round(added_ga,3))+'_added_aba'+str(round(added_aba,3))+'_ic_'+str(val_ic)
                    #print(string_pars)
                    subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                    dirout = dataname_out+'/'+subfolder_out

                    filedat=dirout+'/'+'obs.dat'

                    x_obs = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=['added_aba','added_ga','fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size'])
                    pdval_ic=pd.DataFrame({'val_ic': val_ic},index=[0])

                    x_obs= pd.concat([pdval_ic, x_obs], axis=1)

                    if ind==0 :
                        dose_obs=x_obs
                    else :
                        dose_obs=pd.concat([dose_obs,x_obs])
                    ind=ind+1
        filedat=dataname_out+'/dose_obs.dat'
        dose_obs.to_csv(path_or_buf=filedat, sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

        os.chdir(dirini)

    if plot_doses_analysis==1 :
        subprocess.call("./"+pyscripts+"/"+script_analysis_doses,shell=True)    # necessary for creating the dictionaries
