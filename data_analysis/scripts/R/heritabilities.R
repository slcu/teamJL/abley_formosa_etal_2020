library(tidyverse)
library(janitor) # to quickly clean column names


# Prepare data ------------------------------------------------------------

# read tidy data
magics <- read_csv("data/MAGICs.csv") %>%
  clean_names()

# calculate traits per experimental unit
magics_sum <- magics %>%
  # remove experiments with less than 10 seeds germinated
  filter(total_germinated > 10) %>%
  # retain only experiments with ~30 days after ripening
  filter(mean_dar > 25 & mean_dar < 45) %>%
  # this gives a unique combination of seed packet (i.e. what went on a petri-dish)
  group_by(magic_line, plant, mother_plant_sowing_number, mean_dar, id) %>%
  # calculate traits
  summarise(
    median = median(rep(day, number), na.rm = TRUE),
    mean = mean(rep(day, number), na.rm = TRUE),
    mode = mean(day[which(number == max(number))]),
    var =  var(rep(day, number), na.rm = TRUE),
    sd = sqrt(var),
    cv = sd/mean,
    ls = mean(abs(rep(day, number) - mean)/mean, na.rm = TRUE),
    range = diff(range(rep(day, number), na.rm = TRUE)) + 1,
    rel_range =range /mean,
    iqr = quantile(rep(day, number), 0.75, na.rm = TRUE) - quantile(rep(day, number), 0.25, na.rm = TRUE),
    pct_germ = (total_germinated[1] / total[1])*100
  ) %>%
  ungroup()


# Snippet of code to perform quantile normalization
quantnorm<-function(x) {
  n=sum(!is.na(x),na.rm=T)
  x=rank(x)/(n+1)
  x=qnorm(x)
  x[is.infinite(x)]=NA
  x
}

rank_norm <- function(x){
  # replace data by ranks - ties are taken as their integer part
  ranks <- rank(x)

  # sequence of odd numbers divided by 2x sample size
  odds <- seq(1, 2*length(x) - 1, by = 2)/(2*length(x))

  # inverse normal
  ys <- qnorm(odds[ranks])

  return(ys)
}

magics_sum <- magics_sum %>%
  # rank transform (see: https://statmodeling.stat.columbia.edu/2015/07/13/dont-do-the-wilcoxon/)
  mutate(cv_rn = rank_norm(cv),
         mean_rn = rank_norm(mean),
         median_rn = rank_norm(median),
         mode_rn = rank_norm(mode),
         pct_rn = rank_norm(pct_germ))




# Fit model ---------------------------------------------------------------

# there's several levels of variation nested on each other:
# Level1: id (the individual packet of seeds stored for a certain number of days)
# Level2: plant (the individual plant that generates a packet of seeds)
# Level3: mother_plant_sowing_number (the mom of the individual in the previous level)
# Level4: magic_line (the MAGIC line ID)

# Fit multi-level model to account for all these levels
get_variance <- function(d, trait){
  # scale the trait
  d$.y <- (d[[trait]] - mean(d[[trait]], na.rm = TRUE))/sd(d[[trait]], na.rm = TRUE)
  m_fit <- lme4::lmer(.y ~ (1|magic_line/mother_plant_sowing_number/plant),
                      data = d)

  # get variance estimates
  m_fit %>%
    lme4::VarCorr() %>%
    as_tibble() %>%
    mutate(prop = vcov/sum(vcov)) %>%
    mutate(trait = trait)
}

map_dfr(c("median", "mean", "mode", "cv", "pct_germ",
          "median_rn", "mean_rn", "mode_rn", "cv_rn", "pct_rn"),
        ~ get_variance(magics_sum, .)) %>%
  ggplot(aes(trait, prop)) +
  geom_col(aes(fill = grp))


vars <- map_dfr(c("median", "mean", "mode", "cv", "pct_germ",
          "median_rn", "mean_rn", "mode_rn", "cv_rn", "pct_rn"),
        ~ get_variance(magics_sum, .))


# prettier summary table
trait_names <- tribble(
  ~trait, ~name,
  "cv", "CV",
  "mean", "Mean",
  "median", "Median",
  "mode", "Mode",
  "pct_germ", "Percent",
  "pct", "Percent"
)

vars %>%
  arrange(trait) %>%
  mutate(pct = prop*100) %>%
  filter(grp == "magic_line") %>%
  mutate(transformed = ifelse(str_detect(trait, "_rn"),
                              "Rank-transformed", "Original")) %>%
  mutate(trait = str_remove(trait, "_rn")) %>%
  full_join(trait_names) %>%
  select(name, transformed, pct) %>%
  pivot_wider(names_from = transformed, values_from = pct) %>%
  knitr::kable()


# some really low heritabilities on the original scale
# this might be because of those outliers
magics_sum %>%
  mutate(magic_line = fct_reorder(magic_line, median)) %>%
  ggplot(aes(magic_line, median)) +
  geom_point() +
  theme(axis.title.x = element_blank())

magics_sum %>%
  mutate(magic_line = fct_reorder(magic_line, mode)) %>%
  ggplot(aes(magic_line, mode)) +
  geom_point() +
  theme(axis.title.x = element_blank())

magics_sum %>% filter(mode > 25)
