#!/usr/bin/env python

# This python script executes organism performs parameter sweeps of the ABA/GA model and its subsequent analysis.

#########################################################################################################################

# Parameters and strings to modify by the user

# NOTE: if adding more parameters, remember to modify the sed line.

# Strings to modify
rootsim='../../../Organism/bin/simulator'			 # path in which the organism executable simulator is located
Results_dirout='Results_2pars' # subfolder for simulations
Results_dirout_aux=Results_dirout # leave this aux variable here

Results_dirout_ctl='Results_pars_CTLs_paper'  # subfolder for CTL simulations


pyscripts='pyscripts_initialization_analysis_and_plotting' # scripts subfolder name
dimsweep=2

# The elements of listpar1 and listpar2 contains the pairs of parameter values. The first parameter space  exploration will be the first listpar1 parameter versus the first listpar2 parameter value, and so on.
listpar1=['theta_zga','theta_zaba','basal_target','basal_target','theta_zaba','basal_ga','theta_aba','basal_aba']
listpar2=['seed_vol','theta_zga','deg_target','deg2_target','seed_vol','deg_ga','theta_ga','deg_aba']

# Most important general script parameters

sims=1   # sims=1 runs organism simulation, sims=0 does not run organism. NOTE: when sims=0, findroots is not executed, so ic are set to 0 in the json file.
find_roots=1    # set 1 to find roots of the dynamics - needed to set appropriate initial conditions.

do_single_simulation_analysis=1 # set it to 1 to analyse single simulations, and allow doing plottings of it.
get_obs=1                        # set it to 1 to extract the main outcomes and export it to obs.dat files, otherwise set it to 0 (setting it to 0 will prevent doing the parameter sweep analysis and plots, unless these obs.dat files have been created beforehand)
plotting_individual_simulations_figs=1 # set it to 1 to analyse single simulations, and allow doing plottings of it.

do_par_sweep_analyses=1   # set it to 1 to plot the parameter sweep analyses, otherwise set it to 0.
extract_obs_from_sweep=1   # set it 1 to put together the data for the 2D parameter plots (ie compiling obs.dat files to produce the final dose_obs.dat); otherwise put it to 0 (eg in case the dose_obs.dat file is already there because it has been previously generated)
plot_2dpars_projection=1  # set it to 1 to produce the 1D plots from the 2D parameter explorations

plot_theolines=0 # set it to 0 for now.
get_bifdiag=0  # set it to 0 here. Bifurcation diagrams for parameter sweeps have not been implemented, yet.

#######

# Simulation parameters

# Model parameters

seed_vol=30         # V parameter, inversely proportional to dynamic noise amplitude

# SEE WITHIN THE LOOP FOR SETTING NUMBER OF SEEDS; 40 FOR CTL (WITHOUT THE RISE OF GA PRODUCTION), AND 400 FOR THE 'NORMAL' SIMULAIONS
#num_seeds=400  # number of seeds
end_time=1000 # final simulation time


num_vars=8 # number of variables - important for the generation of the init file

bastimes=[0,0.01] # list of coefficient values in the GA equation for the CreationOne reaction (bastime_ga). Setting one of this values to 0 would involve not rising GA protduction before sowing (ie., CTL simulation)
#bastimes=[0.01] # list of coefficient values in the GA equation for the CreationOne reaction (bastime_ga). Setting one of this values to 0 would involve not rising GA protduction before sowing  (ie., CTL simulation)

added_ga=0   # exogenous GA
added_aba=0  # exogenous ABA

# basal productions
basal_aba=1.0    # basal production for ABA
basal_ga=0.3     # basal production for GA
basal_target=0.3 # basal production for the Integrator
basal_m=39       # basal production for the Modulator (called Z in the paper)

# degradations
deg_aba=1  # ABA degradation
deg_ga=1   # GA degradation
deg_m=0.1  # degradation rate for Modulator (called Z in the paper)
deg_target=0.4 # Integrator degradation

# coefficients for the regulatory hill functions
deg2_target=6 # coefficient in the GA-mediated degradation of Integrator
vmax_aba=10   # coefficient of Hill term in ABA equation
vmax_ga=4   # coefficient of Hill term in GA equation
vmax_z=10   # coefficient of Hill term in Integrator equation

# thresholds in the hill functions for ABA, GA and Integrator
theta_aba=3.7    # 'threshold' in hill function for activation of ABA by Integrator
theta_ga=1.2     # 'threshold' in hill function for inhibition of GA by Integrator

theta_zaba=6.5 # 'threshold' in hill function for biosynthesis of Integrator by ABA
theta_zga=6     # 'threshold' in hill function for degradation of Integrator mediated by GA

# Hill exponents
hhh=4           # cooperativity for ABA and GA Hill functions
h_1=hhh      # Activator hill exponent for promotion of Integrator accumulation by ABA
h_2=hhh      # Hill exponent for promotion of GA-induced degradation

thresholds_for_germination=2

# static noise parameters (to omit in these simulations, set them to 0)
delta_1aba=0
delta_2ga=0


# Initial conditions set to zero here, but afterwards in the script they are assigned to the corresponding fixed points.
ga_ic=0
aba_ic=0
target_ic=0

# More general script parameters. Set them to 1 to perform its associated task, otherwise, set them to 0.

generate_ic=1   # generate_ic=1 generate new initial conditions from the fixed points of the dynamics

if sims==0 :
    generate_ic=0
    #find_roots=0

# Other strings to modify

solver='hi'			  			 # solver file (eg., rk, hi and gi) that is going to be used,

model='zabaga_doses_v0'              # model file name without the '.model' extension

script_analysis='analysis_zabaga_results_paper.py'

if dimsweep==1 :
    script_analysis_par_sweep='analysis_vs_par_paper.py'
elif dimsweep==2 :
    script_analysis_par_sweep='analysis_vs_2pars_paper_reformat2.py'

string_sim='2021-04-01_' # Substring of the subfolder where the results are going to be stored

plot_histos=1            # plot_histos=1 plots initial and final histograms of the simulated variables, plot_histos=0 does not plot it
plot_timecourses=1       # plot_timecourses=1 plots time courses of the simulated variables, plot_timecourses=0 does not plot it
plot_phasediag=1         # plot_phasediag=1 plots the nullclines of the dynamical system with and without trajectiories
plot_timecourse_germination_fraction=0
plot_timecourses_productionga=0
plot_target_vs_gaaba=0   # plot_target_vs_gaaba=1 plots the target versus the aba/ga ratio

aspect_histos=0.45


##############################################################################################################
##### Importing packages #####################################################################################

# Importing packages
import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np
import pylab
import os
import shutil
import subprocess
import pickle
import json
import pandas as pd

##############################################################################################################


dirini=os.getcwd()  # storing the path in which the run_sepals.py file is located

ind_phd=0

for ky in range(0,len(listpar1)) :
    par1=listpar1[ky]
    par2=listpar2[ky]

    for kz in range(0,len(bastimes)) :
        bastime_ga=bastimes[kz]

        pardict={'varying_par':{'par1': par1,'par2': par2,'dimsweep': dimsweep},'var_par_list': {'basal_aba': 'vs_basal_aba','basal_ga': 'vs_basal_ga','theta_zaba': 'vs_theta_zaba','theta_zga': 'vs_theta_zga','theta_aba': 'vs_theta_aba','theta_ga': 'vs_theta_ga','deg_aba':'vs_deg_aba','deg_ga':'vs_deg_ga','bastime_ga':'vs_bastime_ga','basal_m':'vs_basal_m','deg_target':'vs_deg_target','deg2_target':'vs_deg2_target','basal_target': 'vs_basal_target','seed_vol': 'vs_seed_vol','vmax_z':'vs_vmax_z'},'par_string_list':{'basal_aba':'bas_aba','basal_ga': 'bas_ga','theta_zaba':'the_zaba','theta_zga': 'the_zga','theta_aba':'the_aba','theta_ga': 'the_ga','deg_aba':'deg_aba','deg_ga':'deg_ga','bastime_ga':'basz_ga','basal_m':'bas_m','basal_target':'basal_target','deg_target':'deg_target','deg2_target':'deg2_target','seed_vol': 'seed_vol','vmax_z':'vmax_z'},'label_list':{'basal_target':'Integrator Production','basal_aba':'ABA production','basal_ga': 'GA production','deg_aba':'ABA degradation','deg_ga':'GA degradation','theta_zaba' :'I threshold to ABA','theta_zga': 'I threshold to GA','theta_aba' :'GA threshold to ABA','theta_ga': 'ABA threshold to GA','bastime_ga':'GA Biosynthesis Coefficient','basal_m':'GA Biosynthesis Coefficient','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','seed_vol': 'Seed Effective Volume','vmax_z':'ABA-dependent I activation'}}
        if dimsweep==1 :
            parsweepdict={'parsweep':{'basal_aba':[0.01,0.1,1,3,4,5,6,7],'theta_zaba':[0.01,0.1,1,3,5,6,7,8,9,10],'theta_zga':[7,8],'basal_m':[1,10,20,30,32,36,40,50,75,100],'deg2_target':[0.01,0.1,1,3,5,6,7,8,9,10],'seed_vol':[1,10,20,30],'thresholds_for_germination':[2]}}
        elif dimsweep==2 :
            lsweep3=np.ndarray.tolist(np.logspace(-1, 2, num=10))
            lsweep2=np.ndarray.tolist(np.logspace(0, 2, num=9))
            lsweep2bis=np.ndarray.tolist(np.logspace(0, 2, num=11))
            lsweep3m1=np.ndarray.tolist(np.logspace(-1, 2, num=16))
            lsweep3m2=np.ndarray.tolist(np.logspace(-2, 1, num=16))
            lsweep2m1=np.ndarray.tolist(np.logspace(-1, 1, num=11))
            lsweep4=np.ndarray.tolist(np.logspace(-2, 2, num=21))

            parsweepdict={'parsweep':{'basal_target':lsweep3m1,'deg_target':lsweep3m2,'deg2_target':lsweep4,'basal_aba':lsweep4,'basal_ga':lsweep4,'theta_zaba':lsweep4,'theta_zga':lsweep4,'theta_aba':lsweep4,'theta_ga':lsweep4,'deg_aba':lsweep4,'deg_ga':lsweep4,'basal_m':[1,10,20,30,32,36,40,50,75,100],'seed_vol':lsweep2bis,'thresholds_for_germination':[2]}}
            if (ky> 2) and (ky< 8) :
                parsweepdict={'parsweep':{'basal_ga':lsweep3m2,'deg_ga':lsweep3m2,'basal_aba':lsweep3m2,'deg_aba':lsweep3m2,'basal_target':lsweep3m1,'deg_target':lsweep3m2,'deg2_target':lsweep3m1,'theta_zaba':lsweep3m2,'theta_zga':lsweep4,'theta_aba':lsweep4,'theta_ga':lsweep3m1,'basal_m':[1,10,20,30,32,36,40,50,75,100],'seed_vol':lsweep2,'thresholds_for_germination':[2]}}

        pardict.update(parsweepdict)

        list_par1=parsweepdict['parsweep'][par1]
        list_par2=parsweepdict['parsweep'][par2]

        # Lines to automatically determine the exogenous exploration we are doing, depending the length of the ABA and GA exogenous concentrations lists.
        num_par1=len(list_par1)
        num_par2=len(list_par2)

        if num_par1>0 :
            end_time=1000
            ticks_separation=200
            num_print_times=int(end_time/10.0)+1
            auxnum_print_times=int(end_time/1000.0)+1
            end_plotting_time=end_time
        if num_par2>0 :
            num_seeds=400
            list_ics=[1,2,3]

            num_print_times=int(end_time/1000.0)+1
            auxnum_print_times=int(end_time/1000.0)+1
            plot_timecourses=0
            plotting_individual_simulations_figs=1

        if bastime_ga==0 :
            num_print_times=int(end_time/1000.0)+1
            auxnum_print_times=int(end_time/1000.0)+1
            Results_dirout=Results_dirout_ctl
            num_seeds=40
            list_ics=[1]
            do_par_sweep_analyses=1
            plotting_individual_simulations_figs=0
        else :
            Results_dirout=Results_dirout_aux # for output that is not CTL simulations


        num_realisations=len(list_ics)
        end_plotting_time=1000
        ticks_separation=200

        os.chdir(dirini) # going back to the abaga directory

        fullmodelfile='ModelFiles/'+model+'.model'
        fullsolverfile='SolverFiles/'+solver

        # Model parameters
        try:
            os.stat(Results_dirout)
        except:
            os.mkdir(Results_dirout) # creating the directory

        par_string=pardict['par_string_list'][par1]
        var_par=pardict['var_par_list'][par1]+'_'+pardict['par_string_list'][par2]
        label_par=pardict['label_list'][par1]

        #  Note: for simplicity, I took out from the dataname string for now the following : +'_hhh_'+str(hhh)+'_h_1_'+str(h_1)+'_h_2_'+str(h_2)    ;  +'_del_1aba'+str(delta_1aba) ;    +'_del_2ga'+str(delta_2ga)
        dataname=var_par+'_seed_vol'+str(seed_vol)+'_deg_aba'+str(deg_aba)+'_deg_ga'+str(deg_ga)+'_bas_ga'+str(basal_ga)+'_vmax_aba'+str(vmax_aba)+'_vmax_ga'+str(vmax_ga)+'_the_aba'+str(theta_aba)+'_the_ga'+str(theta_ga)+'_bas_targ'+str(basal_target)+'_deg_targ'+str(deg_target)+'_vmaxz_'+str(vmax_z)+'_the_zaba'+str(theta_zaba)+'_deg2_target_'+str(deg2_target)+'_the_zga'+str(theta_zga)+'_basal_m_'+str(basal_m)+'_deg_m_'+str(deg_m)+'_bastime_ga_'+str(bastime_ga)+'_num_seeds'+str(num_seeds)+'_end_time'+str(end_time)+'_threshold'+str(thresholds_for_germination)
        dataname_out=Results_dirout+'/'+dataname
        xnum_seeds=40
        dataname_CTL=var_par+'_seed_vol'+str(seed_vol)+'_deg_aba'+str(deg_aba)+'_deg_ga'+str(deg_ga)+'_bas_ga'+str(basal_ga)+'_vmax_aba'+str(vmax_aba)+'_vmax_ga'+str(vmax_ga)+'_the_aba'+str(theta_aba)+'_the_ga'+str(theta_ga)+'_bas_targ'+str(basal_target)+'_deg_targ'+str(deg_target)+'_vmaxz_'+str(vmax_z)+'_the_zaba'+str(theta_zaba)+'_deg2_target_'+str(deg2_target)+'_the_zga'+str(theta_zga)+'_basal_m_'+str(basal_m)+'_deg_m_'+str(deg_m)+'_bastime_ga_'+str(0)+'_num_seeds'+str(xnum_seeds)+'_end_time'+str(end_time)+'_threshold'+str(thresholds_for_germination)
        CTL_dataname_out=Results_dirout_ctl+'/'+dataname_CTL

        print(dataname)

        try:
            os.stat(dataname_out)
        except:
            os.mkdir(dataname_out) # creating the directory

        try:
            os.stat(Results_dirout+'/phds_summaries/')
        except:
            os.mkdir(Results_dirout+'/phds_summaries/') # creating the directory

        try:
            os.stat(Results_dirout+'/sims_summaries/')
        except:
            os.mkdir(Results_dirout+'/sims_summaries/') # creating the directory

        try:
            os.stat(Results_dirout+'/sims_summaries_stats/')
        except:
            os.mkdir(Results_dirout+'/sims_summaries_stats/') # creating the directory

        # This line is to make sure I can recover the used python script of a runned simulation
        if (sims==1) :
            shutil.copyfile('run_zabaga_scan_parameters.py',dataname_out+'/run_zabaga_scan_parameters.py')

        info={'initial_conds_pars':{'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times}, \
        'model_pars': {'num_vars' : num_vars, 'deg_aba' : deg_aba, 'deg_ga' : deg_ga, 'deg_m':deg_m,'basal_aba' : basal_aba, 'basal_ga' : basal_ga, 'basal_m':basal_m, 'bastime_ga':bastime_ga,'vmax_aba' : vmax_aba, 'vmax_ga' : vmax_ga, 'theta_aba' : theta_aba, 'theta_ga' : theta_ga , 'deg_target' : deg_target, 'basal_target' : basal_target, 'vmax_z' : vmax_z, 'deg2_target' : deg2_target, 'theta_zaba': theta_zaba, 'theta_zga': theta_zga, 'delta_1aba': delta_1aba, 'delta_2ga': delta_2ga, 'hhh': hhh, 'h_1': h_1, 'h_2': h_2,'seed_vol':seed_vol}, \
        'plotting_pars': {'plotting_individual_simulations_figs': plotting_individual_simulations_figs,'get_bifdiag':get_bifdiag, 'plot_timecourses_productionga':plot_timecourses_productionga,'plot_histos' : plot_histos, 'aspect_histos' : aspect_histos,'plot_timecourses' : plot_timecourses,'plot_phasediag' : plot_phasediag, 'plot_target_vs_gaaba' : plot_target_vs_gaaba,'plot_timecourse_germination_fraction': plot_timecourse_germination_fraction,'plot_theolines':plot_theolines,'end_plotting_time':end_plotting_time,'ticks_separation':ticks_separation,'get_obs': get_obs}, \
        'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'CTL_main_parsexploration_path' : CTL_dataname_out,'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim},'doses_pars':{'added_ga' : added_ga, 'added_aba' : added_aba}}

        info.update(pardict)


        for index_par1 in range(0,len(list_par1),1) :
            par1val=list_par1[index_par1]
            for index_par2 in range(0,len(list_par2),1) :

                par2val=list_par2[index_par2]
                os.chdir(dirini) # going back to the abaga directory

                # Generating a dictionary of parameters and storing them in a file

                info['model_pars'][par1]=par1val
                info['model_pars'][par2]=par2val

                if find_roots==1 :
                    # creating a json to pass parameters to the findroots script
                    with open('info.json','w') as f:
                        json.dump(info, f)

                    print('executing findroots')
                    command0="./"+pyscripts+"/findroots_vs_par.py"
                    subprocess.call(command0,shell=True)

                    # moving output to the results subfolder

                    # reading back the results, to get the fixed points
                    with open("info.json", "r") as f:
                        info= json.load(f)

                    print(info['initial_conds_pars'])

                    aba_ic=info['initial_conds_pars']['aba_ic']
                    ga_ic=info['initial_conds_pars']['ga_ic']
                    target_ic=info['initial_conds_pars']['target_ic']

                dataname_init='aba_ic_'+str(round(aba_ic,3))+'_ga_ic_'+str(round(ga_ic,3))+'_target_ic_'+str(round(target_ic,2))+'_thgerm_'+str(thresholds_for_germination)+'_num_seeds_'+str(num_seeds)+'_num_vars_'+str(num_vars) #

                initfilename=dataname_init;

                newinfo={'initial_conds_pars':{'ga_ic' : ga_ic, 'aba_ic' : aba_ic, 'target_ic' : target_ic,'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times, 'start_ic_seed' : list_ics[0]},'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'CTL_main_parsexploration_path' : CTL_dataname_out,'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim,'init_filename':initfilename}}

                info.update(newinfo)
                with open('info.json','w') as f:
                    json.dump(info, f)

        		# creating the output folder in case it does not exist
                if generate_ic==1 :
                    subprocess.call("./"+pyscripts+"/initfile_generator_organism_nonspatial_zabaga.py",shell=True)

                for ics in range(0,len(list_ics),1) :
                    os.chdir(dirini)
                    val_ic=list_ics[ics]

                    if dimsweep==1 :
                        string_pars=info['strings']['var_par']+str(round(par1val,3))+'_ic_'+str(val_ic)
                    elif dimsweep==2 :
                        string_pars=info['par_string_list'][par1]+str(round(par1val,3))+info['par_string_list'][par2]+str(round(par2val,3))+'_ic_'+str(val_ic)

                    # this conditional has been introduced to allow to have the first ics with higher printed time resolution than the other ics.
                    if ics>1 :
                        num_print_times=auxnum_print_times
                        newinfo={'initial_conds_pars':{'ga_ic' : ga_ic, 'aba_ic' : aba_ic, 'target_ic' : target_ic,'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times},'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'CTL_main_parsexploration_path' : CTL_dataname_out, 'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim,'init_filename':initfilename}}

                        info.update(newinfo)
                        with open('info.json','w') as f:
                            json.dump(info, f)

                    subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                    dirout = dataname_out+'/'+subfolder_out

                    try:
                        os.stat(dirout)
                    except:
                        os.mkdir(dirout)

                    with open(dirout+'/info.json','w') as f:
                        json.dump(info, f)

                    if (sims==1) :

                        initfile='InitFiles/'+dataname_init+'/'+initfilename+'_ic_'+str(val_ic)+'.init'
                        shutil.copyfile(initfile,dataname_out+'/'+initfilename+'_ic_'+str(val_ic)+'.init')	# moving data init file into the output folder

                        outfile=model+'_'+solver+'_num_out'

                        # introducing parameters into the model file
                        command0="sed -e 's/degaba/"+str(info['model_pars']['deg_aba'])+"/g' -e 's/degga/"+str(info['model_pars']['deg_ga'])+"/g' -e 's/degm/"+str(deg_m)+"/g' -e 's/basalaba/"+str(info['model_pars']['basal_aba'])+"/g' -e 's/basalga/"+str(info['model_pars']['basal_ga'])+"/g' -e 's/bastimega/"+str(info['model_pars']['bastime_ga'])+"/g' -e 's/basalm/"+str(info['model_pars']['basal_m'])+"/g' -e 's/vmaxaba/"+str(vmax_aba)+"/g' -e 's/vmaxga/"+str(vmax_ga)+"/g' -e 's/thetaga/"+str(info['model_pars']['theta_ga'])+"/g' -e 's/thetaaba/"+str(info['model_pars']['theta_aba'])+"/g' -e 's/degtarget/"+str(info['model_pars']['deg_target'])+"/g' -e 's/basaltarget/"+str(info['model_pars']['basal_target'])+"/g' -e 's/vmaxz/"+str(info['model_pars']['vmax_z'])+"/g' -e 's/deg2target/"+str(info['model_pars']['deg2_target'])+"/g' -e 's/thetazaba/"+str(info['model_pars']['theta_zaba'])+"/g' -e 's/thetazga/"+str(info['model_pars']['theta_zga'])+"/g' -e 's/delta1aba/"+str(delta_1aba)+"/g' -e 's/delta2ga/"+str(delta_2ga)+"/g' -e 's/hhh/"+str(hhh)+"/g' -e 's/h1/"+str(h_1)+"/g' -e 's/h2/"+str(h_2)+ "/g' -e 's/addedga/"+str(added_ga)+"/g' -e 's/addedaba/"+str(added_aba)+"/g' ModelFiles/pre_"+model+".model >  ModelFiles/"+model+".model"
                        subprocess.call(command0,shell=True)

                        # introducing parameters into the solver file
                        command0="sed -e 's/seedvol/"+str(info['model_pars']['seed_vol'])+"/g' -e 's/endtime/"+str(end_time)+"/g' -e 's/numprinttimes/"+str(num_print_times)+"/g' SolverFiles/pre_"+solver+" >  SolverFiles/"+solver
                        subprocess.call(command0,shell=True)

                        initfilename_extended=initfilename+'_ic_'+str(val_ic)+'.init'
                        fullinitfile='InitFiles/'+dataname_init+'/'+initfilename_extended
                        outfile='num_out'

                        cmd=rootsim+' '+fullmodelfile+' '+fullinitfile+' '+fullsolverfile #+'  > '+dirout+'/'+outfile  # command line for running organism
                        print('Running '+cmd)
                        subprocess.call(cmd,shell=True)

                        shutil.copyfile(fullmodelfile,dirout+'/'+model+'.model')		# copying model file
                        shutil.move('organism.gdata',dirout+'/organism.gdata')					    # moving output to the results subfolder
                        shutil.copyfile(initfile,dirout+'/'+initfilename_extended)				# copying init file to the results subfolder
                        shutil.copyfile(fullsolverfile,dirout+'/'+solver)				# copying integrator file to the results subfolder

                        # plotting loop

                    if do_single_simulation_analysis==1 :
                        print('Doing single simulation analysis')

                        outfile=model+'_'+solver+'_num_out'
                        subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                        dirout = dataname_out+'/'+subfolder_out
                        shutil.copy2(pyscripts+"/"+script_analysis,dirout+'/'+script_analysis)
                        shutil.copy2(pyscripts+"/"+"eqs.py",dirout+'/eqs.py')

                        os.chdir(dirini+'/'+dirout)
                        subprocess.call("./"+script_analysis,shell=True)	# necessary for creating the dictionaries

        if do_par_sweep_analyses==1 :
            os.chdir(dirini)

            with open(dataname_out+'/'+'info.json','w') as f:
                json.dump(info, f)

            if dimsweep==1 :
                colnames=[par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size']
            elif dimsweep==2 :
                colnames=[par1,par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']

            print("Doing parameter exploration analyses")
            # putting together all the simulation results into a panda and exporting this to the dose_obs file
            if extract_obs_from_sweep==1 :
                ind=0
                for index_par1 in range(0,len(list_par1),1) :
                    par1val=list_par1[index_par1]
                    for index_par2 in range(0,len(list_par2),1) :
                        par2val=list_par2[index_par2]
                        for ics in range(0,len(list_ics),1) :
                            os.chdir(dirini)
                            val_ic=list_ics[ics]

                            if dimsweep==1 :
                                string_pars=info['strings']['var_par']+str(round(par1val,3))+'_ic_'+str(val_ic)
                            elif dimsweep==2 :
                                string_pars=info['par_string_list'][par1]+str(round(par1val,3))+info['par_string_list'][par2]+str(round(par2val,3))+'_ic_'+str(val_ic)

                            #print(string_pars)
                            subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                            dirout = dataname_out+'/'+subfolder_out

                            filedat=dirout+'/'+'obs.dat'

                            x_obs = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=colnames)
                            pdval_ic=pd.DataFrame({'val_ic': val_ic},index=[0])

                            x_obs= pd.concat([pdval_ic, x_obs], axis=1)

                            if ind==0 :
                                dose_obs=x_obs
                            else :
                                dose_obs=pd.concat([dose_obs,x_obs])
                            ind=ind+1
                filedat=dataname_out+'/dose_obs.dat'
                dose_obs.to_csv(path_or_buf=filedat, sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

            os.chdir(dirini)
            if dimsweep==2 and plot_2dpars_projection==1 :
                #subprocess.call("./"+pyscripts+"/"+"plot_2dpars_projection_loops.py",shell=True)    # necessary for creating the dictionaries
                #subprocess.call("./"+pyscripts+"/"+"plot_2dpars_projection_loops_easytocompare.py",shell=True)    # necessary for creating the dictionaries
                #subprocess.call("./"+pyscripts+"/"+plot_2dpars_projection_loops_coloursregions.py",shell=True)    # necessary for creating the dictionaries
                subprocess.call("./"+pyscripts+"/"+"plot_2dpars_projection_loops_excluding_allnonbio_points.py",shell=True)    # necessary for creating the dictionaries

            subprocess.call("./"+pyscripts+"/"+script_analysis_par_sweep,shell=True)    # necessary for creating the dictionaries
            if bastime_ga>0 :
                subprocess.call("./"+pyscripts+"/analysis_across_par_sweep_reformat_excluding_germbeforesowing.py",shell=True)    # necessary for creating the dictionaries

                colnames=['mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region']
                x_stats = pd.read_csv(dataname_out+'/'+'stats.dat',sep='\t',skip_blank_lines=True,names=colnames)

                colnames_ics=['val_ic','mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region']
                x_stats_ics = pd.read_csv(dataname_out+'/'+'stats_ics.dat',sep='\t',skip_blank_lines=True,names=colnames_ics)

                if ind_phd==0 :
                    phds_stats=x_stats
                    phds_stats_ics=x_stats_ics

                else :
                    phds_stats=pd.concat([phds_stats,x_stats])
                    phds_stats_ics=pd.concat([phds_stats_ics,x_stats_ics])
        if bastime_ga>0 :
            ind_phd=ind_phd+1

if (do_par_sweep_analyses==1 and bastime_ga>0) :
    filedat=Results_dirout+'/phds_stats.dat'
    phds_stats.to_csv(path_or_buf=filedat, sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')
    filedat=Results_dirout+'/phds_stats_ics.dat'
    phds_stats_ics.to_csv(path_or_buf=filedat, sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

    subprocess.call("./"+pyscripts+"/"+"analysis_stats_phds.py",shell=True)    # necessary for creating the dictionaries
