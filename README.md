# Code for Abley & Formosa-Jordan et al 2021

This repository holds the data analysis and simulations code for:

Katie Abley\*\*, Pau Formosa-Jordan\*\*, Hugo Tavares, Emily YT Chan, Mana Afsharinafar, Ottoline Leyser\*, James C.W. Locke\* (2021) "An ABA-GA bistable switch can account for natural variation in the variability of Arabidopsis seed germination time" ([link](https://doi.org/10.7554/eLife.59485))

*Corresponding authors  
**These authors contributed equally  
Email: James.Locke@slcu.cam.ac.uk (JCWL)  
Email: ol235@cam.ac.uk (OL)  

Note that version v0.1 of this code relates to a previous bioRxiv manuscript ([link](https://www.biorxiv.org/content/10.1101/2020.06.05.135681v1)).

All of the code (and data) is deposited as a static snapshot in the Cambridge Apollo repository (_doi link to be added once published_).

The code in the GitHub repository is non-static and may be updated if we find any bugs.
If you encounter problems running this code, please [open a new issue](https://gitlab.com/slcu/teamJL/abley_formosa_etal_2020/-/issues) in the repository.


## Instructions

Clone the repository (or [download as a zip file](https://gitlab.com/slcu/teamJL/abley_formosa_etal_2020/-/archive/master/abley_formosa_etal_2020-master.zip) and extract the files) - you should then have a project directory called _"abley_formosa_etal_2020"_.

The repository holds code for two main parts: 

* [`data_analysis`](data_analysis) holds data analysis code, which recreates panels for Figs 1-4, 6B, 7 and their supplements.
* [`modelling`](modelling) holds modelling code, which recreates Figs 5-6 and their supplements. 

Detailed instructions is given in the README of each sub-folder.

## License

The code is published under a [GNU GPLv3 licence](LICENSE.md). 
