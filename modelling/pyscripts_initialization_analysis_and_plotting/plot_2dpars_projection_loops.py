#!/usr/bin/env python

# Importing packages###############
import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio
matplotlib.rcParams['pdf.fonttype'] = 42
#import statistics

#############################################
fiz=1.77       # Figure size      # Figure size
fiz=2.05
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots

dirini=os.getcwd()

#with open("../info.json", "r") as f:
#    info= json.load(f)
#os.chdir("..")

with open("info.json", "r") as f:
    info= json.load(f)

#os.chdir("..")

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_parsexploration_path']
num_realisations=info['initial_conds_pars']['num_realisations']
#var_doses=info['strings']['var_doses']
string_sim=info['strings']['string_sim']
par1=info['varying_par']['par1']
par2=info['varying_par']['par2']
dimsweep=info['varying_par']['dimsweep']

var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']

get_bifdiag=info['plotting_pars']['get_bifdiag']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

bastime_ga=info['model_pars']['bastime_ga']

par1sweep=info['parsweep'][par1]
par2sweep=info['parsweep'][par2]

ymin=min(par1sweep)
ymax=max(par1sweep)
xmin=min(par2sweep)
xmax=max(par2sweep)

#if bastime_ga>0 :
#    if 'CTL_dataname_out' in locals():
#        colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']
#        CTL_filedat=CTL_dataname_out+'/dose_obs.dat' 
#        CTL_x = pd.read_csv(CTL_filedat,sep='\t',skip_blank_lines=True,names=colnames)

filedat=dataname_out+'/dose_obs.dat' 

if dimsweep==1 :
    colnames=['val_ic',par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size']
elif dimsweep==2 :
    colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']

x = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=colnames)
x=x[(x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)]

x.cv_gt=x.cv_gt.replace(0, np.nan) # replacing CVs being 0 by nans.

pranges={'fraction_germinated_seeds':[0, 1],'cv_gt':[0, x['cv_gt'].max()],'mode_gt':[0, x['mode_gt'].max()],'mean_gt':[0, x['mean_gt'].max()]}
lpranges={'cv_gt':[-1.5, np.log10(x['cv_gt'].max())],'mode_gt':[-1, np.log10(x['mode_gt'].max())]}

ics=x['val_ic'].unique()

labels={'fraction_germinated_seeds':'Germination fraction','mean_gt':'Mean germination time (A.U.)','added_ga':'Exogenous GA','added_aba':'Exogenous ABA','cv_gt':'CV of germination time ','q2_gt':'Median germination time (A.U.)','cvq_gt': 'IQR/median of germination time','mode_gt':'Mode germination time (A.U.)','size':'Number of germinated seeds','theta_zga':'GA threshold for integrator','theta_zaba':'ABA threshold for integrator'}

# Labels for paper
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds','theta_zga':r'$\theta_{I,GA}$','theta_zaba':r'$\theta_{I,ABA}$','seed_vol':'V','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','basal_target':'I Production'}
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds','theta_ga':r'$\theta_{GA}$','theta_aba':r'$\theta_{ABA}$','basal_aba':'ABA Production','basal_ga':'GA Production','deg_aba':'ABA Degradation','deg_ga':'GA Degradation','theta_zga':r'$\theta_{I,GA}$','theta_zaba':r'$\theta_{I,ABA}$','seed_vol':'V','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','basal_target':'I Production'}
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds'}
axlabels={'theta_ga':'I threshold \n for GA inhibition ' r'($\theta_{GA,I})$','theta_aba':'I threshold \n for ABA activation ' r'$(\theta_{ABA,I})$','basal_aba':'ABA production ' r'($\beta_{ABA}$)','basal_ga':'GA production \n' r'($\beta_{GA}$)','deg_aba':'ABA degradation ' r'($\nu_{ABA}$)','deg_ga':'GA degradation ' r'($\nu_{GA}$)','theta_zga': 'GA threshold for \n I degradation ' r'($\theta_{I,GA})$','theta_zaba': 'ABA threshold \n for I activation ' r'($\theta_{I,ABA})$','seed_vol':'1/(Noise intensity) ' r'($V$)','deg2_target':'GA-dependent \n I degradation ' r'($C_{I,GA}$)','deg_target': 'I degradation ' r'($\nu_{I})$','basal_target': 'I production ' r'($\beta_{I}$)'}
axlabels={'theta_ga':'I threshold \n for GA inhibition ' r'($\theta_{GA,I})$','theta_aba':'I threshold \n for ABA activation ' r'$(\theta_{ABA,I})$','basal_aba':'ABA production \n' r'($\beta_{ABA}$)','basal_ga':'GA production \n' r'($\beta_{GA}$)','deg_aba':'ABA degradation \n' r'($\nu_{ABA}$)','deg_ga':'GA degradation \n' r'($\nu_{GA}$)','theta_zga': 'GA threshold for \n I degradation ' r'($\theta_{I,GA})$','theta_zaba': 'ABA threshold \n for I activation ' r'($\theta_{I,ABA})$','seed_vol':'1/(Noise intensity) \n' r'($V$)','deg2_target':'GA-dependent \n I degradation ' r'($C_{I,GA}$)','deg_target': 'I degradation \n' r'($\nu_{I})$','basal_target': 'I production \n' r'($\beta_{I}$)'}
filelabels={'fraction_germinated_seeds':'fr_germ_seeds','mean_gt':'mean_gt','q2_gt':'median_gt','cv_gt':'cv_gt','cvq_gt':'cvq_gt','mode_gt':'mode_gt','size':'gseedsnum'}


variables_to_plot=['fraction_germinated_seeds','mean_gt','cv_gt','mode_gt']
variables_to_plot=['fraction_germinated_seeds','cv_gt','mode_gt']

pars2=x[par2].unique()
pars1=x[par1].unique()

# Plotting figures
fig_size=(fiz,fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'

#rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Arial']
pylab.rcParams.update(params)

dirout = dataname_out+'/vs_1par_plots'  #creating sub-directory
try:
    os.stat(dirout)
except:
    os.mkdir(dirout)

fig_size=2


tosave=1


partochange=par1
parctt=par2

parctts=x[parctt].unique()

xs_sweep=x[partochange].unique()
zs_sweep=x[parctt].unique()

#xs_sweep=info['parsweep'][partochange]
#zs_sweep=info['parsweep'][parctt]

parctts=zs_sweep


for jj in range(0,len(parctts),1) :                   #for loop for specific variables
    indx=jj
    print(str(parctt),'=', parctts[indx])

    for ii in range(0,len(variables_to_plot),1) :                   #for loop for specific variables
        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        #plt.subplots_adjust(left=0.25, bottom=0.20, right=0.8, top=None, wspace=0.5, hspace=0.6)
        plt.subplots_adjust(left=0.25, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        var=variables_to_plot[ii]    #get name of variable 
        xmin=min(xs_sweep);
        xmax=max(xs_sweep)*1.1;
        ymin=0;ymax=max(x[var])*1.05;
        
        #scale='linear'
        for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
            ind=ics[jj]                   #seed_indices defined at start
            xs=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][partochange].values      #x values of time course for one seed
            ys=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][var].values      #ind is an index

            plt.plot(xs,ys,'-o',linewidth=lw);

        plt.xlabel(axlabels[str(partochange)],fontsize=fz)
        plt.ylabel(labels[var],fontsize=fz)
        

        #plt.xticks(np.logspace(-2,2,num=5), (r"0",r"2",r"4",r"6",r"8",r"10"),fontsize=fz)
        
        #if partochange=='seed_vol' :
        #    plt.xticks(np.logspace(-2,2,num=5),fontsize=fz)
        #else :
        #    plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)
  
        plt.yticks(np.arange(0,end_plotting_time,step=ticks_separation),fontsize=fz)

        #plt.setp(ax.get_xticklabels(), fontsize=fz)
        #plt.xticks(np.arange(0,0.04,step=0.02))
        #ScalarFormatter(useOffset=True)

        rat = (ymax-ymin)/(xmax-xmin)

        if var=='cv_gt' :
            plt.yscale('log');
            ymax=1
            ymin=0.03
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))

            #plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)

        if var=='fraction_germinated_seeds' :
            ymax=max(x[var])*1.1
            plt.yticks(np.arange(0,1.01,step=0.25),(r"0",r"25",r"50",r"75",r"100"),fontsize=fz)

        if var=='mean_gt' :
            ymax=end_plotting_time*0.95

        if var=='mode_gt' :
            ymin=1

            plt.yscale('log');
            ymax=end_plotting_time*0.95
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))

        plt.xscale('log');
        #ax1.xaxis.grid(True, which='minor')
        
        plt.minorticks_off()
        #ax1.set_aspect(0.618/rat)
        ax1.set_aspect(1.0/rat)

        plt.subplots_adjust(bottom=0.5)

        if not(isnan(ymax)) :
            plt.axis([xmin,xmax,ymin,ymax])
        
        plt.tight_layout()

        if tosave==1 :
            fileout='_'+var
            fname=string_sim+par_string+"_dose"+fileout+".pdf"   
            fname=var+'_vs_'+str(partochange)+'_'+str(parctt)+str(parctts[indx])+'.pdf'

            fullfname=dirout+'/'+fname
            plt.savefig(fullfname, format=None)
        
        #plt.show()
        
        plt.close()

partochange=par2
parctt=par1

parctts=x[parctt].unique()

xs_sweep=x[partochange].unique()
zs_sweep=x[parctt].unique()

#xs_sweep=info['parsweep'][partochange]
#zs_sweep=info['parsweep'][parctt]

parctts=zs_sweep

for jj in range(0,len(parctts),1) :                   #for loop for specific variables
    indx=jj
    print(str(parctt),'=', parctts[indx])

    for ii in range(0,len(variables_to_plot),1) :                   #for loop for specific variables
        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        plt.subplots_adjust(left=0.25, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        var=variables_to_plot[ii]    #get name of variable 
        xmin=min(xs_sweep);
        xmax=max(xs_sweep)*1.1;
        ymin=0;ymax=max(x[var])*1.05;
        
        scale='log'
        #scale='linear'
        for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
            ind=ics[jj]                   #seed_indices defined at start
            xs=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][partochange].values      #x values of time course for one seed
            ys=x[(x.val_ic==ind) & (x[parctt]==zs_sweep[indx])][var].values      #ind is an index

            plt.plot(xs,ys,'-o',linewidth=lw);

        plt.xlabel(axlabels[str(partochange)],fontsize=fz)
        plt.ylabel(labels[var],fontsize=fz)
        

        #plt.xticks(np.logspace(-2,2,num=5), (r"0",r"2",r"4",r"6",r"8",r"10"),fontsize=fz)
        #plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)
        #if partochange=='seed_vol' :
        #    plt.xticks(np.logspace(-2,2,num=5),fontsize=fz)
        #else :
        #    plt.xticks(np.logspace(-2,2,num=3),fontsize=fz)

        plt.yticks(np.arange(0,end_plotting_time,step=ticks_separation),fontsize=fz)


        #plt.setp(ax.get_xticklabels(), fontsize=fz)
        #plt.xticks(np.arange(0,0.04,step=0.02))
        #ScalarFormatter(useOffset=True)
        rat = (ymax-ymin)/(xmax-xmin)

        if var=='cv_gt' :
            plt.yscale('log');
            ymax=1
            ymin=0.03
            #plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)
            #rat = (log10(ymax)-log10(ymin))/(xmax-xmin)
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))

        if var=='fraction_germinated_seeds' :
            ymax=max(x[var])*1.1
            plt.yticks(np.arange(0,1.01,step=0.25),(r"0",r"25",r"50",r"75",r"100"),fontsize=fz)

        if var=='mean_gt' :
            ymax=end_plotting_time*0.95

        if var=='mode_gt' :
            ymin=1

            plt.yscale('log');
            ymax=end_plotting_time*0.95
            #rat = (log10(ymax)-log10(ymin))/(xmax-xmin)
            rat = (log10(ymax)-log10(ymin))/(log10(xmax)-log10(xmin))

        plt.xscale('log');
        #ax1.xaxis.grid(True, which='minor')
        
        plt.minorticks_off()
        plt.subplots_adjust(bottom=0.5)

        if not(isnan(ymax)) :
            plt.axis([xmin,xmax,ymin,ymax])
        
        plt.tight_layout()

        #ax1.set_aspect(0.618/rat)
        ax1.set_aspect(1.0/rat)
        
        if tosave==1 :
            fileout='_'+var
            fname=string_sim+par_string+"_dose"+fileout+".pdf"   
            fname=var+'_vs_'+str(partochange)+'_'+str(parctt)+str(parctts[indx])+'.pdf'

            fullfname=dirout+'/'+fname
            plt.savefig(fullfname, format=None)
        
        #plt.show()
        
        plt.close()
