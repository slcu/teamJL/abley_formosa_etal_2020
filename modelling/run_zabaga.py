#!/usr/bin/env python

# This python script executes organism for few sets of model and simulation parameters, and runs a script that plots the results.
# The results are generated and stored in a subfolder where this script is located.

#########################################################################################################################

# Parameters and strings to modify by the user

# Strings to modify
rootsim='../../../Organism/bin/simulator'			 # path in which the organism executable simulator is located.
Results_dirout='Results_single_simulations'
pyscripts='pyscripts_initialization_analysis_and_plotting' # scripts subfolder name

# Most important general script parameters
sims=1          # sims=1 runs organism simulation, sims=0 does not run organism.
find_roots=1    # set 1 to find roots of the dynamics - needed to set appropriate initial conditions.

do_single_simulation_analysis=1 # set it to 1 to analyse single simulations, and allow doing plottings of it.
get_obs=1
plotting_individual_simulations_figs=1

plot_theolines=0
plot_individual_simulation_results=1    # plot_results=1 will enable to plot time courses, histograms and trajectiories in the solution space. Setting it to zero will avoid plotting some of them (see conditional "if" below)

seed_vol=30    # V parameter, inversely proportional to dynamic noise amplitude

num_seeds=4000  # number of seeds
end_time=1000   # final simulation time

list_ics=[1,2,3] # tags for initial conditions. If list_ics=[1], there is just 1 initial condition run for each parameter value. If list_ics=[1,2,3], there would be 3 runs.

dimsweep=2   # dimension of the parameter sweep.

#######

# Simulation parameters
# IMPORTANT : see Readme.md to understand the equivalence of these parameters with the model parameters in the manuscript

# Model parameters

num_vars=8 # number of variables - important for the generation of the init file

bastimes=[0.01] # list of coefficient values in the GA equation for the CreationOne reaction (bastime_ga). Setting one of this values to 0 would involve not rising GA biosynthesis (ie., CTL simulation)


added_ga=0   # exogenous GA
added_aba=0  # exogenous ABA

# basal productions
basal_aba=1.0    # basal production for ABA
basal_ga=0.3     # basal production for GA
basal_target=0.3 # basal production for the Integrator
basal_m=39       # basal production for the Modulator (called Z in the paper)

# degradations
deg_aba=1  # ABA degradation
deg_ga=1   # GA degradation
deg_m=0.1  # degradation rate for Modulator (called Z in the paper)
deg_target=0.4 # Integrator degradation

# coefficients for the regulatory hill functions
deg2_target=6 # coefficient in the GA-mediated degradation of Integrator
vmax_aba=10   # coefficient of Hill term in ABA equation
vmax_ga=4   # coefficient of Hill term in GA equation
vmax_z=10   # coefficient of Hill term in Integrator equation

# thresholds in the hill functions for ABA, GA and Integrator
theta_aba=3.7    # 'threshold' in hill function for activation of ABA by Integrator
theta_ga=1.2     # 'threshold' in hill function for inhibition of GA by Integrator

theta_zaba=6.5 # 'threshold' in hill function for biosynthesis of Integrator by ABA
theta_zga=6     # 'threshold' in hill function for degradation of Integrator mediated by GA

# Hill exponents
hhh=4           # cooperativity for ABA and GA Hill functions
h_1=hhh      # Activator hill exponent for promotion of Integrator accumulation by ABA
h_2=hhh      # Hill exponent for promotion of GA-induced degradation

thresholds_for_germination=2

# static noise parameters (to omit in these simulations, set them to 0)
delta_1aba=0
delta_2ga=0

# list of parameters to change in this script [ see embedded for loops to understand how this works, and parsweepdict below for the parameter values ]
listpar1=['basal_target']
listpar2=['deg_target']

parsweepdict={'parsweep':{'basal_target':[0.3],'deg_target':[0.4,1.4]}}


# Initial conditions set to zero here, but afterwards in the script they are assigned to the corresponding fixed points.
ga_ic=0
aba_ic=0
target_ic=0

# More general script parameters. Set them to 1 to perform its associated task, otherwise, set them to 0.

generate_ic=1   # generate_ic=1 generate new initial conditions from the fixed points of the dynamics

if sims==0 :
    generate_ic=0
    #find_roots=0

#find_roots=0

plot_histos=1 			 # plot_histos=1 plots initial and final histograms of the simulated variables, plot_histos=0 does not plot it
plot_timecourses=1  	 # plot_timecourses=1 plots time courses of the simulated variables, plot_timecourses=0 does not plot it
plot_phasediag=1  		 # plot_phasediag=1 plots the nullclines of the dynamical system with and without trajectiories
plot_target_vs_gaaba=0   # plot_target_vs_gaaba=1 plots the target versus the aba/ga ratio
plot_timecourse_germination_fraction=0
plot_timecourses_productionga=0

aspect_histos=0.45

if plot_individual_simulation_results==0 :
    plot_histos=0
    plot_timecourses=0
    plot_target_vs_gaaba=0
    plot_phasediag=0
    plot_timecourses_productionga=0

# Other strings to modify

solver='hi'			  			 # solver file (eg., rk, hi and gi) that is going to be used,

model='zabaga_doses_v0'              # model file name without the '.model' extension

script_analysis='analysis_zabaga_results_paper.py'

string_sim='2020-06-01_' # Substring of the subfolder where the results are going to be stored

num_print_times=int(end_time/2.0)+1
auxnum_print_times=int(end_time/1000.0)+1 # parameter for giving a different num_print_times for ics > 1

num_realisations=len(list_ics)

end_plotting_time=140
ticks_separation=40

##############################################################################################################
##### Importing packages #####################################################################################

# Importing packages
import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np
import pylab
import os
import shutil
import subprocess
import pickle
import json
import pandas as pd

##############################################################################################################


dirini=os.getcwd()  # storing the path in which this script is located

ind_phd=0

for ky in range(0,len(listpar1)) :
    par1=listpar1[ky]
    par2=listpar2[ky]
    pardict={'varying_par':{'par1': par1,'par2': par2,'dimsweep': dimsweep},'par_string_list':{'basal_aba':'bas_aba','basal_ga': 'bas_ga','theta_zaba':'the_zaba','theta_zga': 'the_zga','theta_aba':'the_aba','theta_ga': 'the_ga','deg_aba':'deg_aba','deg_ga':'deg_ga','bastime_ga':'basz_ga','basal_m':'bas_m','basal_target':'basal_target','deg_target':'deg_target','deg2_target':'deg2_target','seed_vol': 'seed_vol'},'label_list':{'basal_target':'Integrator Production','basal_aba':'ABA production','basal_ga': 'GA production','deg_aba':'ABA degradation','deg_ga':'GA degradation','theta_zaba' :'I threshold to ABA','theta_zga': 'I threshold to GA','theta_aba' :'GA threshold to ABA','theta_ga': 'ABA threshold to GA','bastime_ga':'GA Biosynthesis Coefficient','basal_m':'GA Biosynthesis Coefficient','deg2_target':'GA-dependent I degradation','deg_target':'I degradation','seed_vol': 'Seed Effective Volume'}}

    for kz in range(0,len(bastimes)) :
        bastime_ga=bastimes[kz]

        pardict.update(parsweepdict)

        list_par1=parsweepdict['parsweep'][par1]
        list_par2=parsweepdict['parsweep'][par2]

        os.chdir(dirini) # going back to the abaga directory

        fullmodelfile='ModelFiles/'+model+'.model'
        fullsolverfile='SolverFiles/'+solver

        # Model parameters
        try:
            os.stat(Results_dirout)
        except:
            os.mkdir(Results_dirout) # creating the directory

        par_string=pardict['par_string_list'][par1]
        var_par='sim'
        label_par=pardict['label_list'][par1]

        #  Note: for simplicity, I took out from the dataname string for now the following : +'_hhh_'+str(hhh)+'_h_1_'+str(h_1)+'_h_2_'+str(h_2)    ;  +'_del_1aba'+str(delta_1aba) ;    +'_del_2ga'+str(delta_2ga)
        dataname=var_par+'_seed_vol'+str(seed_vol)+'_deg_aba'+str(deg_aba)+'_deg_ga'+str(deg_ga)+'_bas_ga'+str(basal_ga)+'_vmax_aba'+str(vmax_aba)+'_vmax_ga'+str(vmax_ga)+'_the_aba'+str(theta_aba)+'_the_ga'+str(theta_ga)+'_bas_targ'+str(basal_target)+'_deg_targ'+str(deg_target)+'_vmaxz_'+str(vmax_z)+'_the_zaba'+str(theta_zaba)+'_deg2_target_'+str(deg2_target)+'_the_zga'+str(theta_zga)+'_basal_m_'+str(basal_m)+'_deg_m_'+str(deg_m)+'_bastime_ga_'+str(bastime_ga)+'_num_seeds'+str(num_seeds)+'_end_time'+str(end_time)+'_threshold'+str(thresholds_for_germination)
        dataname_out=Results_dirout+'/'+dataname

        print(dataname)

        try:
            os.stat(dataname_out)
        except:
            os.mkdir(dataname_out) # creating the directory

        # This line is to make sure I can recover the used python script of a runned simulation
        if (sims==1) :
            shutil.copyfile('run_zabaga.py',dataname_out+'/run_zabaga.py')

        info={'initial_conds_pars':{'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times}, \
        'model_pars': {'num_vars' : num_vars, 'deg_aba' : deg_aba, 'deg_ga' : deg_ga, 'deg_m':deg_m,'basal_aba' : basal_aba, 'basal_ga' : basal_ga, 'basal_m':basal_m, 'bastime_ga':bastime_ga,'vmax_aba' : vmax_aba, 'vmax_ga' : vmax_ga, 'theta_aba' : theta_aba, 'theta_ga' : theta_ga , 'deg_target' : deg_target, 'basal_target' : basal_target, 'vmax_z' : vmax_z, 'deg2_target' : deg2_target, 'theta_zaba': theta_zaba, 'theta_zga': theta_zga, 'delta_1aba': delta_1aba, 'delta_2ga': delta_2ga, 'hhh': hhh, 'h_1': h_1, 'h_2': h_2,'seed_vol':seed_vol}, \
        'plotting_pars': {'plotting_individual_simulations_figs': plotting_individual_simulations_figs,'plot_timecourses_productionga':plot_timecourses_productionga,'plot_histos' : plot_histos, 'aspect_histos' : aspect_histos,'plot_timecourses' : plot_timecourses,'plot_phasediag' : plot_phasediag, 'plot_target_vs_gaaba' : plot_target_vs_gaaba,'plot_timecourse_germination_fraction': plot_timecourse_germination_fraction,'plot_theolines':plot_theolines,'end_plotting_time':end_plotting_time,'ticks_separation':ticks_separation,'get_obs': get_obs}, \
        'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim},'doses_pars':{'added_ga' : added_ga, 'added_aba' : added_aba}}

        info.update(pardict)

        for index_par1 in range(0,len(list_par1),1) :
            par1val=list_par1[index_par1]
            for index_par2 in range(0,len(list_par2),1) :

                par2val=list_par2[index_par2]
                os.chdir(dirini) # going back to the abaga directory

                # Generating a dictionary of parameters and storing them in a file

                info['model_pars'][par1]=par1val
                info['model_pars'][par2]=par2val

                if find_roots==1 :
                    # creating a json to pass parameters to the findroots script
                    with open('info.json','w') as f:
                        json.dump(info, f)

                    print('executing findroots')
                    command0="./"+pyscripts+"/findroots_vs_par.py"
                    subprocess.call(command0,shell=True)

                    # moving output to the results subfolder
                    # reading back the results, to get the fixed points
                    with open("info.json", "r") as f:
                        info= json.load(f)

                    print(info['initial_conds_pars'])

                    aba_ic=info['initial_conds_pars']['aba_ic']
                    ga_ic=info['initial_conds_pars']['ga_ic']
                    target_ic=info['initial_conds_pars']['target_ic']

                dataname_init='aba_ic_'+str(round(aba_ic,3))+'_ga_ic_'+str(round(ga_ic,3))+'_target_ic_'+str(round(target_ic,2))+'_thgerm_'+str(thresholds_for_germination)+'_num_seeds_'+str(num_seeds)+'_num_vars_'+str(num_vars) #

                initfilename=dataname_init;

                newinfo={'initial_conds_pars':{'ga_ic' : ga_ic, 'aba_ic' : aba_ic, 'target_ic' : target_ic,'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times, 'start_ic_seed' : list_ics[0]},'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim,'init_filename':initfilename}}

                info.update(newinfo)
                with open('info.json','w') as f:
                    json.dump(info, f)

        		# creating the output folder in case it does not exist
                if generate_ic==1 :
                    subprocess.call("./"+pyscripts+"/initfile_generator_organism_nonspatial_zabaga.py",shell=True)

                for ics in range(0,len(list_ics),1) :
                    os.chdir(dirini)
                    val_ic=list_ics[ics]
                    string_pars=info['par_string_list'][par1]+str(round(par1val,3))+info['par_string_list'][par2]+str(round(par2val,3))+'_ic_'+str(val_ic)

                    # this conditional has been introduced to allow to have the first ics with higher printed time resolution than the other ics.
                    if ics>1 :
                        num_print_times=auxnum_print_times
                        newinfo={'initial_conds_pars':{'ga_ic' : ga_ic, 'aba_ic' : aba_ic, 'target_ic' : target_ic,'thresholds_for_germination' : thresholds_for_germination, 'num_seeds': num_seeds, 'num_realisations': num_realisations,'num_print_times' : num_print_times},'strings' : {'main_path' : dirini,'Results_dirout': Results_dirout,'main_parsexploration_path' : dataname_out, 'par_string': par_string, 'var_par' : var_par,'label_par' : label_par,'string_sim': string_sim,'init_filename':initfilename}}

                        info.update(newinfo)
                        with open('info.json','w') as f:
                            json.dump(info, f)

                    subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                    dirout = dataname_out+'/'+subfolder_out
                    #print(subfolder_out)
                    try:
                        os.stat(dirout)
                    except:
                        os.mkdir(dirout)

                    with open(dirout+'/info.json','w') as f:
                        json.dump(info, f)

                    if (sims==1) :

                        initfile='InitFiles/'+dataname_init+'/'+initfilename+'_ic_'+str(val_ic)+'.init'
                        shutil.copyfile(initfile,dataname_out+'/'+initfilename+'_ic_'+str(val_ic)+'.init')	# moving data init file into the output folder

                        outfile=model+'_'+solver+'_num_out'

                        # introducing parameters into the model file
                        command0="sed -e 's/degaba/"+str(info['model_pars']['deg_aba'])+"/g' -e 's/degga/"+str(info['model_pars']['deg_ga'])+"/g' -e 's/degm/"+str(deg_m)+"/g' -e 's/basalaba/"+str(info['model_pars']['basal_aba'])+"/g' -e 's/basalga/"+str(info['model_pars']['basal_ga'])+"/g' -e 's/bastimega/"+str(info['model_pars']['bastime_ga'])+"/g' -e 's/basalm/"+str(info['model_pars']['basal_m'])+"/g' -e 's/vmaxaba/"+str(vmax_aba)+"/g' -e 's/vmaxga/"+str(vmax_ga)+"/g' -e 's/thetaga/"+str(info['model_pars']['theta_ga'])+"/g' -e 's/thetaaba/"+str(info['model_pars']['theta_aba'])+"/g' -e 's/degtarget/"+str(info['model_pars']['deg_target'])+"/g' -e 's/basaltarget/"+str(info['model_pars']['basal_target'])+"/g' -e 's/vmaxz/"+str(vmax_z)+"/g' -e 's/deg2target/"+str(info['model_pars']['deg2_target'])+"/g' -e 's/thetazaba/"+str(info['model_pars']['theta_zaba'])+"/g' -e 's/thetazga/"+str(info['model_pars']['theta_zga'])+"/g' -e 's/delta1aba/"+str(delta_1aba)+"/g' -e 's/delta2ga/"+str(delta_2ga)+"/g' -e 's/hhh/"+str(hhh)+"/g' -e 's/h1/"+str(h_1)+"/g' -e 's/h2/"+str(h_2)+ "/g' -e 's/addedga/"+str(added_ga)+"/g' -e 's/addedaba/"+str(added_aba)+"/g' ModelFiles/pre_"+model+".model >  ModelFiles/"+model+".model"
                        subprocess.call(command0,shell=True)

                        # introducing parameters into the solver file
                        command0="sed -e 's/seedvol/"+str(info['model_pars']['seed_vol'])+"/g' -e 's/endtime/"+str(end_time)+"/g' -e 's/numprinttimes/"+str(num_print_times)+"/g' SolverFiles/pre_"+solver+" >  SolverFiles/"+solver
                        subprocess.call(command0,shell=True)

                        initfilename_extended=initfilename+'_ic_'+str(val_ic)+'.init'
                        fullinitfile='InitFiles/'+dataname_init+'/'+initfilename_extended
                        outfile='num_out'

                        cmd=rootsim+' '+fullmodelfile+' '+fullinitfile+' '+fullsolverfile #+'  > '+dirout+'/'+outfile  # command line for running organism
                        print('Running '+cmd)
                        subprocess.call(cmd,shell=True)

                        shutil.copyfile(fullmodelfile,dirout+'/'+model+'.model')		# copying model file
                        shutil.move('organism.gdata',dirout+'/organism.gdata')					    # moving output to the results subfolder
                        shutil.copyfile(initfile,dirout+'/'+initfilename_extended)				# copying init file to the results subfolder
                        shutil.copyfile(fullsolverfile,dirout+'/'+solver)				# copying integrator file to the results subfolder

                        # plotting loop

                    if do_single_simulation_analysis==1 :
                        print('Doing single simulation analysis')

                        outfile=model+'_'+solver+'_num_out'
                        subfolder_out=string_sim+model+'_'+solver+'_pars_'+string_pars
                        dirout = dataname_out+'/'+subfolder_out
                        shutil.copy2(pyscripts+"/"+script_analysis,dirout+'/'+script_analysis)
                        shutil.copy2(pyscripts+"/"+"eqs.py",dirout+'/eqs.py')

                        os.chdir(dirini+'/'+dirout)
                        subprocess.call("./"+script_analysis,shell=True)	# necessary for creating the dictionaries
