
Modelling code for Abley & Formosa-Jordan et al 2021
------------------------------------------------

This part of the repository holds the modelling code and the analysis of the simulated data for Abley & Formosa-Jordan et al (2021).

The modelling code consists on a stochastic model on the ABA-GA gene regutalory network for studying variability in germination time. 


<h2>Setup </h2>

This current repository version allows to reproduce the modelling results presented in Abley, Formosa-Jordan et al (2021) (link_to_be_added). Note the tagged version v0.1 of the abley_formosa_etal_2020 repository allows to reproduce the modelling figures for the manuscript Abley, Formosa-Jordan et al (2020) in bioRxiv ([link](https://www.biorxiv.org/content/10.1101/2020.06.05.135681v1)).  

- The computational implementation of the model is done by the open source C++ Organism package. For downloading and installing Organism, see https://gitlab.com/slcu/teamHJ/Organism . If the gitlab master branch presents any problem to run the code, please clone the Organism version tagged as '[v0.Abley_Formosa-Jordan_et_al_2020](https://gitlab.com/slcu/teamHJ/Organism/-/releases/v0.Abley_Formosa-Jordan_et_al_2020)'.

- The scripts for running the Organism simulations, the analysis of the simulated data and the plotting can be run with Python 2.7.3. The Python packages that have been used are, most importantly, scipy (version 1.0.0), numpy (version 1.14.0), matplotlib (version 2.1.2), pandas (version 0.20.1) and seaborn (version 0.7.1). In case of not having these packages, you can do it via a package installer, e.g.:
	-	if using pip, type `pip install numpy pandas scipy matplotlib seaborn`
	- 	if using anaconda, type `conda install numpy pandas scipy matplotlib seaborn`  

-  The `rootsim` path in the main scripts (those scripts that starting with the prefix `run`) need to be updated, such that they point to the correct executable of Organism, called `simulator`. This executable is found in within the `Organism/bin/` folder. 

- The scripts named with prefix `run` recreate each figure in the paper. To execute them using the command line, go to the modelling folder and type `python ./nameofthescript.py` and press return. 

- This code has been run in Linux (Ubuntu 14.04 and 16.04) and in Mac (macOS Mojave).

----------

<h2>Simulations code at a glance</h2>

- Simulations and the subsequent analysis of the simulated results are run by the Python scripts whose name starts by the prefix `run_`.

- The main scripts (starting with the prefix `run_`) would do the following:  
	1.- Computation of fixed points of the dynamics.  
	2.- Generation of initialization file (containing initial conditions).  
	3.- Generate model file (from the 'pre_model' file) and solver file (from the 'pre_solver' file).   
	4.- Numerical integration of the dynamics.  
	5.- Analysis and plots of each individual simulation outcome. Outcomes of single simulations are exported in obs.dat files. For the dose responses and parameter sweeps, the ensemble of obs.dat files are compiled dose_obs.dat files.  
	6.- Analysis of the dose responses or the parameter sweeps if applicable, and plotting the outcomes.  
	7.- In the case of the parameter sweeps, statistical analysis across all the 2D parameter explorations, and plotting the outcomes.  


- pyscripts_initialization_analysis_and_plotting folder: folder containing plotting and analysis scripts.  
- ModelFiles folder: this folder contains the model file for Organism. Note some parameters need to be assigned, which is done automatically with the Python scripts.  
- SolverFiles folder: this folder contains the solver model for Organism. Note some parameters need to be assigned, which is done automatically with the Python scripts.  
- InitFile folder: this folder, which is created after running simulations, will contain the initialisation files once they are created. For the simulations run in this paper, different files for the same parameter values will contain the same initial conditions (apart of the 9th and 10th columns in the Init files, which are not relevant for these simulations).  
- Simulation results will be saved with new folders starting with the prefix 'Results'.  

As a start, run the `run_zabaga.py` script for doing simulations for a few parameter values (for simplicity, the parameter sweep analysis is omitted in this script).

<h2>Further details on scripts and outcomes </h2>

The following scripts can reproduce the modelling figures in Abley & Formosa-Jordan et al (2021). Yet, that given the stochastic nature of the model, and that the used random numbers change every time simulations are run, panels will not be exactly the published ones, but should be similar in statistical terms. Also, note some of the published panels were manually edited to add figure labels and change minor aesthetic aspects.

The execution of the following code will produce more plots than those present in Abley & Formosa-Jordan et al (2021). 

- `run_zabaga_scan_Fig5.py`:  this script producess all panels needed for Figure 5 and Figure 5-figure supplement 1.
Panels for these figures are within the folder 'Results_Fig5/vs_theta_zaba(...)'.  
  	- Figure 5:
		* Figure 5B panels are in the 'vs_1par_plots_biobio' subfolder, which are the files that contain '_vs_theta_zaba' (see parameters nomenclature below).  
		* Figure 5C panels are in the 'summary_single_simulation_plots' subfolder.  
		* Figure 5D panels are in the '2d_plots' subfolder. Files 'swplot_CV_the_zaba_vs_the_ga_bio.pdf' , 'swplot_fr_the_zaba_vs_the_ga_bio.pdf' and 'swplot_Mode_the_zaba_vs_the_ga_bio.pdf' (note they are the files ending with 'bio.pdf', such that the non-biological points are excluded).  

	- Figure 5-figure supplement 1 : go into the 'Results_Fig5/vs_theta_zaba.../' folder, and then you need to search within the specific simulation subfolder (the relevant theta_zaba parameter value is in the string as 'the_zaba'+'parametervalue'). Subfolders ending with the substring 'ic_1' will have a greater time resolution output, which will produce more accurate time courses plots. Plots derived from nullcline analysis are named as phasespace_log....pdf. Time courses are in a subfolder named 'timecourses'. Histograms are in subfolder names 'histos'.  

<br />

- `run_zabaga_scan_parameters.py` : script for parameter sweeps. This script produces all panels needed for Figure 5-figure supplements 2-5. Panels for these figures are within the folder 'Results_2pars'. The subfolders are: 'vs_par1_par2_listofparameters', where 'par1' and 'par2' are the two main parameters that have been changed (see below the parameters nomenclature).

	- Figure 5-figure supplement 2 : see files in the subfolder 'vs_1par_plots_biobio' within the corresponding 'vs_par1_par2_(...)' subfolder. Find the corresponding files starting with 'cv_gt', 'mode_gt' and 'fraction_germinated_seeds'.
	- Figure 5-figure supplement 3 : see files in 'summary_single_simulations_plots' subfolder within the corresponding 'vs_par1_par2_(...)' subfolder.
	- Figure 5-figure supplement 4 : see files in '2d_plots' within the corresponding 'vs_par1_par2_(...)' subfolder. Files starting with  'lc_cv', 'lc_mode' and 'fr' are the logarithmic CV, logarithmic mode and the percentage of germinated seeds diagrams, respectively. The same file names preceded with cbar are equivalent diagrams with colourbar. The theoretical regions plot are files named as following 'phd_par1_vs_par2.pdf', being 'par1' and 'par2' the two explored parameters.
	- Figure 5-figure supplement 5 :  see folder 'Results_2pars_paper/sims_summaries'. The file names start with 'swplot_CV_' , 'swplot_fr_' and 'swplot_Mode_' followed by the relevant parameter explorations, and end with 'bio'. The 'bio' means that non-biological points are removed. 

<br />

- `run_zabaga_doses_aba_v2.py` and `run_zabaga_doses_ga_v2_x-label_reformat.py` : scripts for studying the dose dependence (exogenous ABA and GA). These scripts produce all panels needed for Figure 6, Figure 6-figure supplements 1-3 (see important note below for the GA doses). After running these simulations, see Results_doses. Subfolders start with 'aba_doses' or 'ga_doses', depending if is an exogenous ABA or GA dose application. The difference between the high and low variability lines is in the values of ABA threshold for I production (theta_zaba parameter in the scripts), which is indicated in the subfolder name.   
	- Figure 6 : see files in 'dose_responses_plots' within the corresponding aba doses subfolder. 
	- Figure 6-figure supplement 1 : see files in 'dose_responses_plots' within the ga corresponding doses subfolder (see important note below).  
	- Figure 6-figure supplement 2 : see files with histograms in 'summary_single_simulations_plots' within the corresponding doses subfolder.  
	- Figure 6-figure supplement 3 : see files in 'nullcline_plots' within the corresponding doses subfolder.  


	Important note for the GA doses, please follow the next steps:
	1) Run the `run_zabaga_doses_ga_v2_x-label_reformat.py` script once with the default parameters (and in particular, having sims=1, and plot_doses_analysis=0)  
	2) Open the output file dose_obs.dat, and change those values in the third column that are 0 (representing the mock experiment, without exogenous GA) to 0.01. This will allow a proper represenation of the doses with a logarithmic scale while including the 0 point (the plotting script will amend the labelling from 0.01 to 0). Save the modified file as 'dose_obs_layout_for_plotting.dat' in the same folder where 'dose_obs.dat' was found. 
	3) Run `run_zabaga_doses_ga_v2_x-label_reformat.py` again, but now with parameters sims=0, plot_doses_analysis=1 and do_single_simulation_analysis=0.  


<h3> Further information on the model and the outcome  </h3>

**Important note**  

Target variable in this code refers to the Integrator variable in the Abley & Formosa-Jordan et al (2021) manuscript, and it is also referred as "z". Modulator variable in this code (e.g. see model file) is the Z variable in the manuscript, which is responsible for the rise of GA production.

**Parameters nomenclature:**

In this repository we use several strings to refer to spefic parameters in Abley, Formosa-Jordan et al (2021). These are the main strings used:

basal_aba : basal production for ABA, β_{ABA}  
basal_ga : basal production for GA, β_{GA}  
basal_target: basal production for Integrator, β_{I}  
bastime_ga : basal production for the Integrator, β_{GA,Z}   
basal_m : basal production for the Modulator (called Z in the paper), β_Z   

deg_aba : ABA degradation, v_{ABA}   
deg_ga:  GA degradation, v_{GA}  
deg_m : degradation rate for Modulator (called Z in the paper), v_{Z}  
deg_target: Integrator degradation, v_{I}  

deg2_target : coefficient in the GA-mediated degradation of Integrator, C_{I,GA}  
vmax_aba : coefficient of Hill term in ABA equation, C_{ABA,I}  
vmax_ga : coefficient of Hill term in GA equation, C_{GA,I}  
vmax_z : coefficient of Hill term in Integrator equation, C_{I,ABA}  

theta_aba (also referred as the_aba) : Integrator concentration 'threshold' in the Hill function for activation of ABA, θ_{ABA,I}  
theta_ga  (also referred as the_ga):  Integrator concentration 'threshold' in the Hill function for inhibition of GA by Integrator, θ_{GA,I}  

theta_zaba  (also referred as the_zaba): ABA concentration 'threshold' in the Hill function for production of Integrator, θ_{I,ABA}   
theta_zga (also referred as the_zga): GA concentration 'threshold' in the Hill function for degradation of Integrator mediated,  θ_{I,GA}  

hhh : Hill exponents of all regulatory functions, h  
seed_vol: effective volume of the system, V  


**Further details regarding the 2D parameter explorations**

* dose_obs.dat file  
	This files can be found within the corresponding 'vs_par1_par2_(...)' subfolder, and it has different observables for each parameter scan or dose response. 
	
	Relevant columns for the parameter scan :  
	first column is ‘initial condition’  
	second column  is y-parameter  
	third column  is x-parameter  
	fourth column  is fraction of germinated seeds  
	fifth column is cv  
	last column is the number of fixed points after rise of GA  

<br />

Within the 2d_plots within the corresponding 'vs_par1_par2_(...)' subfolder, there are other files that might be of interest, especially to check the agreement of the simulations with the predicted theoretical zones:  

* phd_sims_mode:
	It has the following new features:  
	- yellow region: region where there is germination. It should not go much into the blue region (ie the non-germination region in the deterministic limit)  
	- solid black line: line defining region where germination is 100%  
	- dotted line: enclosing region where germination occurs in first time step - should match with the magenta region of spontaneous germination.  
	- dashed line: enclosing the region where mode is 40 (ie the same timescale for the rise GA production for the chosen parameter sets)  
<br />

* phd_sims_cv: 
	idem than phd_sims_cv but instead of modes, there are red isoclines representing cv:	
	- CV=0.1 dotted line
	- CV=0.2 dashed-dotted line
	- CV=0.5 dashed line

<br />

**Other outputs** 

When running `run_zabaga_scan_parameters.py` and `run_zabaga_scan_Fig5.py` files, note that a 'Results_pars_CTLs' folder is also generated. This folder contains the output of simulations in which there is not the initial rise of GA production (i.e., bastime=0). Such simulated results will be used to discard simulated points that are not biologically relevant in the 'Results_2pars' corresponding folder.

