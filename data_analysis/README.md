# Data analysis code for Abley & Formosa-Jordan et al 2020

This part of the repository holds the data analysis code for [Abley & Formosa-Jordan et al 2020](). 

## Setup

1. Clone the repository (or [download as a zip file](https://gitlab.com/slcu/teamJL/abley_formosa_etal_2020/-/archive/master/abley_formosa_etal_2020-master.zip) and extract the files) - you should then have a project directory called _"abley_formosa_etal_2020"_.
2. Download and extract the zip files with the [data](https://doi.org/10.17863/CAM.66984) from the Appolo repository. Place them inside the `data_analysis` directory. 

You should then have this directory structure: 

```
abley_formosa_etal_2020
    ├── data_analysis
    |     ├── data
    |     └── scripts
    └── modelling
          └── ...
```

## Running the analysis

Most of the data analysis was done using `R`. 

To recreate the analysis and figures for the paper, open the `2020-Abley_Germination.Rproj` RStudio project file (which will ensure you have the correct working directory set - R scripts use relative paths from the project directory).

Some details about the files on `scripts/R`:

- All scripts named with prefix `Fig` recreate each figure in the paper. Some of the graphs were slightly edited in inkscape for the published version.
- The scripts in `data_processing` do different data processing steps and produce files already provided in the `data` folder in our repository. However, they can be used to recreate those files. 
  - `bsa_run_scans.R` - run the bulk segregant mapping analysis using [QTLSeqr](https://github.com/bmansfeld/QTLseqr).
  - `qtl_run_scans.R` - fit the QTL models on the MAGIC lines using the [MagicHelpR](https://github.com/tavareshugo/MagicHelpR) package. 
  - `GerminationData_MakingLongTables.R` - takes the raw phenotype data and reshapes it into a suitable format for figures. 
- The scripts in `functions` contain some custom functions used in some of the analysis (they are loaded from other scripts).

----

Whole-genome sequence data for bulk-segregant mapping of the _No x Col-0_ F2 was analysed using stand-alone command line tools. 

The code for this analysis is in `scripts/shell`. The scripts are numbered in the order they should be run. We appologise that some of the paths are hard-coded, but the analysis should be fully documented in the code. 

The main output of these scripts is the file `ColxNo_all_naive.tsv` (SNP calls using [freebayes](https://github.com/ekg/freebayes)' naive variant calling method), which we provide in the data repository. 


## Further details

The following data files are provided in our repository:

```
Data/
├── BSAmapping
│   ├── ColxNo_all_naive.tsv
│   ├── QTLseqr_input.tsv
│   └── QTLseqr_results.csv
├── ColxNo_F2germ.csv
├── ColxNo_F3germ.csv
├── cyp707a1_ga3oxMutantsGerm.csv
├── GAandABAdoseResponse.csv
├── germ_summaryForExpComparison.csv
├── germ_summaryForExpComparisonCV.csv
├── MAGICParents.csv
├── MAGICParentsRawData.csv
├── MAGICs.csv
├── MAGICsRawData.csv
├── MutantsDec2020.csv
├── QTLmapping
│   ├── germ_summaryPerLineForQTLMapping.tsv
│   ├── magic_gen_object.rds
│   ├── qtl_scan_all.csv
│   └── qtl_scan_no_outlier.csv
├── SingleSiliquesGermRawData.csv
├── Soil_vs_plates.csv
├── soil_vs_platesRawData.csv
├── SpanishAccessions.csv
├── SpanishAccessionsRawData.csv
└── SummaryCVSowingTimes.csv
```

#### Figures 1 & 3

Data files `MAGICsRawData.csv`, `MAGICParentsRawData.csv` and `SpanishAccessionsRawData.csv` contain germination time data for the MAGIC lines, the MAGIC parental accessions and the Spanish accessions respectively. `soil_vs_platesRawData.csv` contains germination time data for MAGIC lines and accessions used in the soil vs plate comparison in Fig.1 Figure Supplement 1G.

These files were read into the `GerminationData_MakingLongTables.R` script, which converts the daily germination counts into a long format for further analysis. This output of this script was used to generate the following files: `MAGICs.csv`, `MAGICParents.csv`, `SpanishAccessions.csv`, `Soil_vs_plates.csv`.

These files were read into `Fig01_03_MAGICdataAnalysis.R` which was used to summarise the data and perform the analyses used to generate Fig. 1 and its supplements and Fig. 3 and its supplements. The `Fig01_03_MAGICdataAnalysis.R` script outputs the file `germ_summaryPerLineForQTLMapping.tsv` (file found in `data/QTLmapping`) which contains summary statistics for each MAGIC line used for QTL mapping. This script also uses the files `germ_summaryForExpComparison.csv`, `germ_summaryForExpComparisonCV.csv` and `SummaryCVSowingTimes.csv` for intermediate steps in the analysis used to generate Fig. 1C (see comments in script `Fig01_03_MAGICdataAnalysis.R` for full details).

#### Figure 2

`SingleSiliquesGermRawData.csv` contains germination time data for single and half siliques and was analysed with the script `Fig02_SingleandHalfSiliques.R` to generate the plots for Fig.2 and Fig.2, Figure Supplement 1.

#### Figure 4

Figure 4 was generated using files `QTLmapping/qtl_scan_no_outlier.csv`, `QTLmapping/qtl_scan_all.csv` and `BSAmapping/QTLseqr_results.csv` using `Fig04-qtl_bsa_mapping.R`.

Figure 4, Figure Supplement 1 was generated by using the files `QTLmapping/qtl_scan_no_outlier.csv` and `QTLmapping/qtl_scan_all.csv` using `Fig04-S01-qtl_scans.R`.

Figure 4, Figure Supplement 2 was generated by using the files `QTLmapping/qtl_scan_no_outlier.csv`, `QTLmapping/germ_summaryPerLineForQTLMapping.tsv`, `QTLmapping/magic_gen_object.rds` and `MAGICandAccessions/germ_summaryAccessions.tsv` using `Fig04-S02-qtl_effects.R`.

Figure 4, Figure Supplement 3 was generated by using data file `ColxNo_F2germ.csv` with R script `Fig04_Sup3_Col x No F2germ.R`.

Figure 4, Figure Supplement 4 was generated by using data file `ColxNo_F3germ.csv` with R script `Fig04_Sup4_ColxNoF3_germination.R`.

Figure 4 Figure Supplement 5 was generated using data file  `MutantsDec2020.csv` with R script `Fig04_Sup5_QTLCandidateMutants.R`.


#### Figure 6

Figure 6B and its supplements containing experimental data (Figure 6, Figure Supplement 1B; Figure 6, Figure Supplement 4) were generated by using data file `GAandABAdoseResponse.csv` with script `Fig06_Fig06Sup1_ABAandGA_doseResponse.R`.

#### Figure 7

Figure 7 was generated by using data file `cyp707a1_ga3oxMutantsGerm.csv` with the script `Fig07_GA_ABAmutants_germ.R`

